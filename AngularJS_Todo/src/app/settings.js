(function () {
	'use strict';

	angular
		.module('app')
		.constant('settings', angular.extend({}, {
			loginPageUrl: '/login/Login.aspx',
			indexMode: true,
			refreshAuthTokenIntervalMinutes: 10,
			'API_URL': 'http://localhost:3333/api'
		}, window.PUBLISH_SETTINGS)); // eslint-disable-line

}());
