(function(){
	'use strict';

	angular.module('app')
		.controller('modalCtrl', defaultModalCtrl);

	defaultModalCtrl.$inject = ['$modalInstance', 'data'];
	function defaultModalCtrl($modalInstance, data) {
		var vm = this;
		vm.close = close;
		vm.confirm = confirm;
		vm.modalOptions = {};

		angular.extend(vm.modalOptions, data);

		function close() {
			$modalInstance.dismiss();
		}

		function confirm() {
			$modalInstance.close(vm.modalOptions.inputValue);
		}
	}

}());
