(function () {
	'use strict';

	angular
	.module('app')
	.config(routes);

	routes.$inject = ['$stateProvider', '$urlRouterProvider'];
	function routes($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('shell', {
				url: '',
				abstract: true,
				controller: 'shellCtrl',
				controllerAs: 'vm',
				templateUrl: 'app/pages/shell.template.html'
			})
			.state('shell.todos', {
				url: '/todos',
				title: 'To-Do List',
				templateUrl: 'app/pages/todos/todos.template.html',
				controller: 'todosCtrl',
				controllerAs: 'vm',
				resolve: {
					categoriesQ: ['todoApiService', function(todoApiService){
						return todoApiService.getCats();
					}]
				}
			})
			.state('shell.todos.filter', {
				url: '/:id?taskId',
				templateUrl: 'app/pages/tasks/tasks.template.html',
				controller: 'tasksCtrl',
				controllerAs: 'vm',
				resolve: {
					tasksQ: ['$stateParams', 'todoApiService', function($stateParams, todoApiService){
						return todoApiService.getTasks($stateParams.id);
					}]
				},
				// ============================================
				// Use parent state's promises to resolve title
				// ============================================
				onEnter: ['$stateParams', 'titleProvider', 'categoriesQ', 'tasksQ', '$state', function($stateParams, titleProvider, categoriesQ, tasksQ, $state){
					var taskId = $stateParams.taskId,
						categoryId = $stateParams.id;

					if(angular.isDefined(taskId)) {
						var task = _.find(tasksQ.data, function(x){
							return x.id === parseInt(taskId, 10);
						});
						if(angular.isUndefined(task)) {
							$state.go('shell.todos.filter', {id: categoryId, taskId: null});
						}
						else {
							titleProvider.setTitle(task.title);
						}
						return;
					}
					if(angular.isDefined(categoryId)) {
						var cat = _.find(categoriesQ.data, function(x){
							return x.id === parseInt(categoryId, 10);
						});
						if(angular.isUndefined(cat)) {
							$state.go('shell.todos');
						}
						else {
							titleProvider.setTitle(cat.title);
						}
						return;
					}

					titleProvider.setTitle('Categories');
				}]
			});

		$urlRouterProvider.otherwise(function($injector, $location){
			$injector.get('$state').go('shell.todos');
		});
	}
}());
