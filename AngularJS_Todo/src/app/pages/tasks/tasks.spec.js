/// <reference path="../../testHelpers/_reference.js" />
/// <reference path="tasks.controller.js" />
(function () {
	'use strict';

	describe('tasksCtrl', function () {
		beforeEach(module('app'));
		beforeEach(module('app.common'));

		var vm,
			$scope,
			tasksQ,
			$q,
			$stateParams,
			$state,
			$rootScope,
			$log,
			todoApiService,
			newTask;

		beforeEach(inject(function ($controller, _$q_, _$rootScope_, _$log_) {
			$log = _$log_;
			$state = {
				transitionTo: function () {
					return true;
				}
			};
			$rootScope = _$rootScope_;
			$scope = $rootScope.$new();
			$q = _$q_;
			todoApiService = {
				addTask: function(task){
					return $q.when({ data: task });
				},
				updateTask: function(task){
					return $q.when({ data: task });
				}
			};
			tasksQ = {
				data: [
					{
						id: 1,
						title: 'Title',
						category: 1
					},
					{
						id: 2,
						title: 'Task 2',
						category: 2
					}
				]
			};
			newTask = {
				id: 3,
				title: 'title',
				parent: 2
			};
			$stateParams = {
				taskId: 1
			};

			vm = $controller('tasksCtrl', {
				$scope: $scope,
				$state: $state,
				$stateParams: $stateParams,
				$log: $log,
				tasksQ: tasksQ,
				todoApiService: todoApiService,
				$rootScope: $rootScope
			});

			spyOn(todoApiService, 'addTask').and.callThrough();
			spyOn($rootScope, '$broadcast');
			spyOn($state, 'transitionTo');
		}));

		it('should properly initialized', function () {
			// Assert
			expect(vm).toBeDefined();
			expect(vm.todos.length).toBe(2);
			expect(vm.activeTask.id).toBe(1);
			expect(vm.isEditorMode).toBe(true);
		});

		it('should redirect to editor page', function () {

			// Act
			vm.editor(newTask.id);
			$rootScope.$digest();

			// Assert
			expect($state.transitionTo).toHaveBeenCalled();
		});

		it('should redirect to updated task', function () {

			// Act
			vm.saveEditor(newTask);
			$rootScope.$digest();

			// Assert
			expect($state.transitionTo).toHaveBeenCalled();
		});

		it('should redirect on close editor', function () {

			// Act
			vm.closeEditor();
			$rootScope.$digest();

			// Assert
			expect($state.transitionTo).toHaveBeenCalled();
		});

		it('should properly add new task', function () {
			// Arrange
			var oldCount = vm.todos.length;

			// Act
			vm.addTask(newTask);
			$scope.$digest();

			// Assert
			expect(todoApiService.addTask).toHaveBeenCalled();
			expect(vm.todos.length).toBe(oldCount + 1);
		});

		it('should accept events when active category has been changed', function () {
			var newTaskCategory = 12;
			// Act
			$rootScope.$emit('category.pull', newTaskCategory);

			// Assert
			expect(vm.activeCategory).toBe(newTaskCategory);
		});

	});

}());
