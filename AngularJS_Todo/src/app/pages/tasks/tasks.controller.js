(function(){
	'use strict';

	angular
		.module('app')
		.controller('tasksCtrl', tasksCtrl);

	tasksCtrl.$inject = ['$scope', '$state', '$stateParams', '$log', 'tasksQ', 'todoApiService', '$rootScope'];

	function tasksCtrl($scope, $state, $stateParams, $log, tasksQ, todoApiService, $rootScope){
		var vm = this,
			editorCatHistory;

		vm.todos = _.reverse(tasksQ.data);
		vm.activeCategory = $stateParams.id;
		vm.addTask = addTask;
		vm.saveEditor = saveEditor;
		vm.closeEditor = closeEditor;
		vm.editor = openEditor;
		vm.activeTask = _.find(vm.todos, function(x){
			return x.id === parseInt($stateParams.taskId, 10);
		});
		vm.isEditorMode = angular.isDefined(vm.activeTask) ? true : false;
		vm.model = {};

		$rootScope.$broadcast('category.active', vm.activeCategory);

		if(vm.isEditorMode){
			$rootScope.$broadcast('todo.editor', true);
		}

		function saveEditor(task){
			todoApiService.updateTask(task).then(function(){
				$rootScope.$broadcast('todo.editor', false);
				$state.transitionTo('shell.todos.filter', {id: task.category});
			}, function(err){
				$log.warn('Unable to update task');
			});
		}

		function closeEditor(){
			$rootScope.$broadcast('todo.editor', false);
			$state.transitionTo('shell.todos.filter', {id: angular.isDefined(editorCatHistory) ? editorCatHistory : vm.activeCategory});
			editorCatHistory = undefined;
		}

		function openEditor(id){
			$rootScope.$broadcast('todo.editor', true);
			$state.transitionTo('shell.todos.filter', {id: vm.activeCategory, taskId: id});
		}

		function addTask(title){
			var newTask = {
				title: title,
				category: vm.activeCategory,
				done: false
			};

			todoApiService.addTask(newTask).then(function(res){
				vm.model.todoTitle = '';
				vm.todos.unshift(res.data);
			}, function(){
				$log.warn('Unable to add task');
			});
		}

		var unsub = $rootScope.$on('category.pull', function(e, id){
			if(angular.isUndefined(editorCatHistory)){
				editorCatHistory = vm.activeCategory;
			}
			vm.activeCategory = id;
		});

		$scope.$on('$destroy', function(){
			unsub();
		});
	}
}());
