/// <reference path="../../testHelpers/_reference.js" />
/// <reference path="todos.controller.js" />
(function () {
	'use strict';

	describe('todosCtrl', function () {
		beforeEach(module('app'));
		beforeEach(module('app.common'));

		var vm,
			$scope,
			categoriesQ,
			$q,
			$state,
			$rootScope,
			$log,
			todoApiService;

		beforeEach(inject(function ($controller, _$q_, _$rootScope_, _$log_) {
			$log = _$log_;
			$state = {
				transitionTo: function () {
					return true;
				}
			};
			$rootScope = _$rootScope_;
			$scope = $rootScope.$new();
			$q = _$q_;
			todoApiService = {
				addCategory: function(task){
					return $q.when({ data: task });
				},
				deleteCategory: function(id){
					return $q.when({ data: true });
				}
			};
			categoriesQ = {
				data: [
					{
						id: 1,
						title: 'Title'
					},
					{
						id: 2,
						title: 'Sub cat'
					}
				]
			};

			vm = $controller('todosCtrl', {
				$state: $state,
				$scope: $scope,
				$rootScope: $rootScope,
				$log: $log,
				todoApiService: todoApiService,
				categoriesQ: categoriesQ
			});

			// spyOn(vm, 'bindHandlers').and.callThrough();

			spyOn(todoApiService, 'addCategory').and.callThrough();
			spyOn(todoApiService, 'deleteCategory').and.callThrough();
			spyOn($rootScope, '$broadcast');
			spyOn($state, 'transitionTo');
			$scope.$digest();
		}));

		it('should properly initialize', function () {
			// Assert
			expect(vm.categories.length).toBe(2);
			expect(vm).toBeDefined();
		});

		it('should add category from service promise', function () {
			// Arrange
			var title = 'title';

			// Act
			vm.addCategory(title);
			$rootScope.$digest();

			// Assert
			expect(vm.categories.length).toBe(3);
		});

		it('should clear modal when add category not from modal', function () {
			// Arrange
			var title = 'title';

			// Act
			vm.addCategory(title);
			$rootScope.$digest();

			// Assert
			expect(vm.model.categoryTitle.length).toBe(0);
		});

		it('should redirect to parent category after child deletion', function () {
			// Arrange

			// Act
			$scope.$emit('category.delete', 1);
			$rootScope.$digest();

			// Assert
			expect($state.transitionTo).toHaveBeenCalled();
		});

		it('should remove category from local storage', function () {
			// Arrange
			var oldCount = vm.categories.length;

			// Act
			$scope.$emit('category.delete', 1);
			$rootScope.$digest();

			// Assert
			expect(vm.categories.length).toBe(oldCount - 1);
		});
	});

}());
