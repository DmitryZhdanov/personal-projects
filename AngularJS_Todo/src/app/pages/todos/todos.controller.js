(function () {
	'use strict';

	angular
		.module('app')
		.controller('todosCtrl', todosCtrl);

	todosCtrl.$inject = ['$state', '$scope', '$rootScope', '$log', 'todoApiService', 'categoriesQ'];

	function todosCtrl($state, $scope, $rootScope, $log, todoApiService, categoriesQ) {
		var handlers = [],
			vm = this;

		vm.model = {};
		vm.categories = _.reverse(categoriesQ.data);
		vm.addCategory = addCategory;
		vm.activeCategory = null;
		vm.editor = false;

		bindHandlers();

		function addCategory(title, parent) {
			var newTask = {
				title: title,
				parent: parent || null
			};
			todoApiService.addCategory(newTask).then(function (res) {
				vm.categories.unshift(res.data);
				if(!parent) {
					vm.model.categoryTitle = '';
				}
				if(parent) {
					$rootScope.$broadcast('category.open', parent);
				}
			}, function(){
				$log.log('Unable to create category');
			});
		}

		function deleteCategory(id) {
			var category = _.find(vm.categories, function(x){
				return x.id === id;
			});
			todoApiService.deleteCategory(id).then(function (res) {
				_.remove(vm.categories, function(x){
					return x.id === id;
				});
				if(angular.isNumber(category.parent)){
					$state.transitionTo('shell.todos.filter', {id: category.parent});
				}
				else {
					$state.transitionTo('shell.todos');
				}
			}, function(){
				$log.log('Unable to remove category');
			});
		}

		function updateCategory(category){
			todoApiService.updateCategory(category).then(function (res) {
				var cat = _.find(vm.categories, function(x){
					return x.id === category.id;
				});
				cat.title = category.title;
			}, function(){
				$log.log('Can\'t remove category');
			});
		}

		function bindHandler(eventName, callback){
			var handler = $scope.$on(eventName, callback);
			handlers.push(handler);
		}

		function bindHandlers(){
			bindHandler('category.active', function(evt, id){
				vm.activeCategory = id;
				if(vm.editor) {
					$rootScope.$broadcast('category.pull', id);
				}
			});
			bindHandler('category.update', function(evt, category){
				updateCategory(category);
			});
			bindHandler('category.delete', function(evt, id) {
				deleteCategory(id);
			});
			bindHandler('category.sub', function(evt, category) {
				addCategory(category.title, category.parent);
			});
			bindHandler('todo.editor', function(evt, isEditor) {
				vm.editor = isEditor;
			});
		}

		$scope.$on('$destroy', function() {
			angular.forEach(handlers, function(unsubscribe){
				unsubscribe();
			});
		});
	}
}());
