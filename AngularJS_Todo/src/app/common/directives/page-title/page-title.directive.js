(function(){
	'use strict';

	angular
		.module('app.common')
		.directive('pageTitle', pageTitle);

	pageTitle.$inject = ['titleProvider'];

	function pageTitle(titleProvider) {
		return {
			link: function(scope, element) {
				function handler(newTitle){
                    element.text(newTitle);
				}

				var watcher = titleProvider.subscribe(handler);

				handler(watcher.current);

				scope.$on('$destroy', function(){
					watcher.unsubscribe();
				});
			}
		};
	}
}());
