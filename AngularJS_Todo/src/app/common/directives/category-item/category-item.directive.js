(function(){
	'use strict';

	angular
		.module('app.common')
		.directive('categoryItem', categoryItem);

	categoryItem.$inject = ['$timeout'];
	function categoryItem($timeout){
		return {
			restrict: 'A',
			scope: {},
			require: '^categoryList',
			link: function(scope, elem, attrs, ctrl){
				var categoryId = attrs.categoryItem;
				function openList(list, arrow){
					list.removeClass('closed');
					list.parent().find('.tree-closed').eq(0).removeClass('tree-closed');
				}
				// ============================
				// Recusively open parent nodes
				// ============================
				if(categoryId === ctrl.active){
					(function expand(parent){
						openList(parent);
						var sub = parent.parents('.category-list.closed');
						return !sub.length ? false : expand(sub);
					}(elem.parent('.category-list.closed')));
				}

				var unsub = scope.$on('category.open', function(e, id){
					if(categoryId === id.toString()) {
						$timeout(function() {
							openList(elem.children('.category-list.closed'));
						}, 0, false);
					}
				});

				scope.$on('$destroy', function(){
					unsub();
				});
			}
		};
	}

}());
