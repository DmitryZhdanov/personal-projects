(function () {
	'use strict';

	angular
		.module('app.common')
		.directive('todoList', taskList);

	taskList.$inject = ['$rootScope'];

	function taskList($rootScope) {
		return {
			restrict: 'E',
			controllerAs: 'vm',
			replace: true,
			scope: {
				todos: '=',
				onEdit: '&',
				isEditor: '@'
			},
			transclude: true,
			controller: function($scope) {
				var vm = this;
				vm.openEditor = function(id){
					vm.onEdit({id: id});
				};
			},
			bindToController: true,
			templateUrl: 'app/common/directives/todo-list/todo-list.directive.html',
			link: link
		};

		function link(scope, elem, attr, ctrl, transcludeFn) {
			var unsub = scope.$watch('ctrl.isEditor', function(){
				ctrl.isEditor = scope.$eval(ctrl.isEditor);
			});

			scope.$on('$destroy', function(){
				unsub();
			});
		}
	}
}());
