(function(){
	'use strict';

	angular
		.module('app.common')
		.directive('todoEditor', todoEditor);

	function todoEditor(){
		return {
			restrict: 'E',
			templateUrl: 'app/common/directives/todo-editor/todo-editor.directive.html',
			scope: {
				activeTask: '=',
				activeCategory: '=',
				onSave: '&',
				onClose: '&'
			},
			bindToController: true,
			controllerAs: 'vm',
			controller: function(){
				var vm = this;
				vm.save = function(){
					vm.activeTask.category = vm.activeCategory;
					vm.onSave({task: vm.activeTask});
				};
			}
		};
	}
}());
