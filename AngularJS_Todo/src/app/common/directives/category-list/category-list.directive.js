(function(){
	'use strict';

	angular
		.module('app.common')
		.directive('categoryList', categoryList);

	categoryList.$inject = ['recursive', '$rootScope', '$log', '$customModal'];

	function categoryList(recursive, $rootScope, $log, $customModal){
		return {
			restrict: 'E',
			controllerAs: 'vm',
			replace: true,
			scope: {
				categories: '=',
				parentCategory: '@',
				active: '@',
				editor: '='
			},
			bindToController: true,
			templateUrl: 'app/common/directives/category-list/category-list.template.html',
			controller: function($scope) {
				var vm = this;

				vm.deleteCategory = deleteCategory;
				vm.editCategory = editCategory;
				vm.addSubGroup = addSubGroup;
				vm.hasChilds = hasChilds;
				vm.pullTodo = pullTodo;

				if(angular.isUndefined(vm.parentCategory)){
					vm.parentCategory = null;
				}

				function pullTodo(id){
					$rootScope.$broadcast('category.active', id);
				}

				function hasChilds(id){
					return _.filter(vm.categories, function(x){
						return x.parent === id;
					}).length > 0;
				}

				function deleteCategory(id) {
					$customModal.open({
						type: 'confirm',
						settings: {
							header: 'Confirm action',
							message: 'Are you sure want to delete this category?',
							buttons: {
								save: 'Yes'
							}
						}
					}).then(function(){
						$rootScope.$broadcast('category.delete', id);
					});
				}

				function editCategory(id) {
					var cat = _.clone(_.find(vm.categories, function(x){
						return x.id === id;
					}));

					$customModal.open({
						type: 'input',
						settings: {
							header: 'Edit category',
							inputValue: cat.title,
							inputPlaceholder: 'Enter new title'
						}
					}).then(function(res){
						cat.title = res;
						$rootScope.$broadcast('category.update', cat);
					});
				}

				function addSubGroup(parentId){
					$customModal.open({
						type: 'input',
						settings: {
							header: 'Create sub category',
							inputPlaceholder: 'Enter sub category name'
						}
					}).then(function(res){
						$rootScope.$broadcast('category.sub', {title: res, parent: parentId});
					}, function(){
						$log.log('Sub category wasn\'t created.');
					});
				}
			},
			compile: function(element) {
				return recursive.compile(element, function(scope, elem, attrs, ctrl, transcludeFn) {
					elem.on('click', '.tree', function(e){
						e.stopPropagation();
						angular.element(this).toggleClass('tree-closed');
						angular.element(this).parent().parent().children('.category-list').toggleClass('closed');
					});
				});
			}
		};
	}


}());
