(function(){
	'use strict';

	angular
		.module('app.common')
		.directive('disableRoute', disableRoute);

	disableRoute.$inject = [];

	function disableRoute(){
		return {
			restrict: 'AE',
			link: function(scope, elem, attr, ctrl){
				attr.$observe('disableRoute', function(val){
					if(scope.$eval(val)){
						elem.on('click', handler);
					}
					else {
						elem.off('click', handler);
					}
				});

				function handler(e){
					e.preventDefault();
				}
			}
		};
	}

}());
