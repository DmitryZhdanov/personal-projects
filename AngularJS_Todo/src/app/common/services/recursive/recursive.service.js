(function(){
	'use strict';

	angular.module('app.common')
		.factory('recursive', recursive);

	recursive.$inject = ['$compile'];
	function recursive ($compile){
			return {
				compile: function(element, link){

					if(angular.isFunction(link)){
						link = { post: link };
					}

					var contents = element.contents().remove();
					var compiledContents;
					return {
						pre: link && link.pre ? link.pre : null,

						post: function(scope, elem){

							if(!compiledContents){
								compiledContents = $compile(contents);
							}

							compiledContents(scope, function(clone){
								elem.append(clone);
							});

							if(link && link.post){
								link.post.apply(null, arguments);
							}
						}
					};
				}
			};
		}
}());
