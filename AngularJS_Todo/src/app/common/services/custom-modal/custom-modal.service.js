(function(){
	'use strict';

	angular
		.module('app.common')
		.service('$customModal', customModal);

	customModal.$inject = ['$modal'];

	function customModal($modal){
		var defaults = {
				controller: 'modalCtrl',
				controllerAs: 'vm'
			},
			modals = {
				'confirm': {
					templateUrl: 'app/modals/confirm-modal.template.html'
				},
				'input': {
					templateUrl: 'app/modals/input-modal.template.html'
				}
			};

		return {
			open: openModal
		};

		function openModal(setup){
			var settings = angular.extend(defaults, modals[setup.type], {
				resolve: {
					data: function(){
						return setup.settings;
					}
				}
			});
			return $modal.open(settings).result;
		}
	}

}());
