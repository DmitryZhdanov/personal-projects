(function(){
	'use strict';

	angular
		.module('app.common')
		.factory('todoApiService', taskService);

	taskService.$inject = ['$http', 'settings'];

	function taskService($http, settings){
		return {
			getCats: getCats,
			getTasks: getTasks,
			addCategory: addCategory,
			deleteCategory: deleteCategory,
			addTask: addTask,
			updateCategory: updateCategory,
			updateTask: updateTask
		};

		function getCats() {
			return $http.get(settings.API_URL + '/categories/', $http.transform(function (result) {
				return _.map(result, function (item) {
					return {
						id: item.id,
						title: item.title,
						parent: item.parent
					};
				});
			}));
		}

		function addCategory(category) {
			return $http({
				method: 'POST',
				url: settings.API_URL + '/categories/',
				data: category
			});
		}

		function deleteCategory(id) {
			return $http({
				method: 'DELETE',
				url: settings.API_URL + '/categories/' + id
			});
		}

		function updateCategory(category) {
			return $http({
				method: 'PUT',
				url: settings.API_URL + '/categories/' + category.id,
				data: category
			});
		}

		function getTasks(id) {
			return $http.get(settings.API_URL + '/tasks?category=' + id, $http.transform(function (result) {
				return _.map(result, function (item) {
					return {
						id: item.id,
						title: item.title,
						done: item.done,
						category: item.category,
						description: item.description
					};
				});
			}));
		}

		function addTask(task){
			return $http({
				method: 'POST',
				url: settings.API_URL + '/tasks',
				data: task
			});
		}

		function updateTask(task){
			return $http({
				method: 'PUT',
				url: settings.API_URL + '/tasks/' + task.id,
				data: task
			});
		}
	}

}());
