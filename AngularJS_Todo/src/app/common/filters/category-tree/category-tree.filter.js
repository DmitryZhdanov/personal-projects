(function(){
	'use strict';

	angular
		.module('app.common')
		.filter('categoryTree', categoryTree);

	function categoryTree (){
		return function(value, parent){
			var filtered = _.filter(value, function(x){
				return x.parent === (parent === null ? null : parseInt(parent, 10));
			});
			return filtered;
		};
	}
}());
