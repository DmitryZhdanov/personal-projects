(function(){
	'use strict';

	angular
		.module('app.common')
		.filter('normalizeTree', normalizeTree);

	function normalizeTree () {
		return function (obj) {
			var normalized = [];
			angular.forEach(obj, function (item) {
				if(item.parent === null) {
					normalized.push({
						id: item.id,
						title: item.title,
						childs: (function nested(id) {
							return _.map(_.filter(obj, function(x) {
									return x.parent === id;
								}), function(x) {
								return {
									id: x.id,
									title: x.title,
									childs: nested(x.id)
								};
							});
						}(item.id))
					});
				}
			});
			return normalized;
		};
	}
}());
