(function () {
	'use strict';
	/*global __dirname*/

	var gulp = require('gulp');

	var jsonServer = require('json-server');
	var server = jsonServer.create();
	var middlewares = jsonServer.defaults();
	var path = require('path');
	var router = jsonServer.router(path.join(__dirname, 'mock_serve/mock.json'));

	gulp.task('mockServe', function () {
		server.use(middlewares);
		server.use(jsonServer.rewriter({
			'/blog/:resource/:id/show': '/:resource/:id'
		}));
		server.use('/api', router);
		server.listen(3333, function () {

		});
	});
}());
