'use strict';
var validator = (function() {
	var types = {},
		messages = [],
		config = {},
		argSeparator = ':',
		Rule = function(validator, message, arg) {
			this.validator = validator;
			this.errorMessage = message;
		};

	function validate(data, prop) {
		var i, j, typeList, currentType, checker, result_ok, ruleArgs;

		messages = [];
		// Validate property
		if(prop && data.hasOwnProperty(prop)) {
			typeList = config[prop];
			if (!typeList) {
				return;
			}
			for (j = 0; j < typeList.length; j++) {
				currentType = typeList[j].split(argSeparator);
				checker = types[currentType[0]];
				ruleArgs = currentType[1];
				result_ok = checker.validator(data[prop], ruleArgs);
				if (!result_ok) {
					messages.push({
						property: i,
						message: checker.errorMessage.format(ruleArgs)
					});
				}
			}
			return hasErrors();
		}

		// Validate object
		for (i in data) {
			if (data.hasOwnProperty(i)) {
				typeList = config[i];

				if (!typeList) {
					continue;
				}
				for (j = 0; j < typeList.length; j++) {
					currentType = typeList[j].split(argSeparator);

					checker = types[currentType[0]];
					ruleArgs = currentType[1];

					if(currentType[0] === 'required') {
						if(data[ruleArgs] && data[ruleArgs].toString().length !== 0) {
							if(!data[i]) {
								messages.push({
									property: i,
									message: 'Required field'
								});
							}
						}
					}
					else {
						result_ok = checker.validator(data[i], ruleArgs);
						if (!result_ok) {
							messages.push({
								property: i,
								message: checker.errorMessage.format(ruleArgs)
							});
						}
					}
				}
			}
		}
		return hasErrors();
	};

	function hasErrors() {
		return messages.length !== 0;
	};

	function addRule(ruleExp, func, msg) {
		var rule = ruleExp.split(argSeparator);

		if(types.hasOwnProperty(rule[0])) {
			console.error('Error: Validator already has rule with name "'+ rule[0] +'".');
			return;
		}
		types[rule[0]] = new Rule(func, msg);
	}
	function ruleExists(ruleName) {
		return Object.keys(types).indexOf(ruleName) !== -1 ? true : false;
	}

	function bindRule(obj) {
		var prop,
			i,
			ruleExpr,
			ruleName,
			ruleArgs;

		for (prop in obj) {
			if(!config[prop]) config[prop] = [];

			if(obj[prop] instanceof Array) {
				for (i = 0; i < obj[prop].length; i++) {
					ruleExpr = obj[prop][i].split(argSeparator);
					ruleName = ruleExpr[0];
					ruleArgs = ruleExpr[1];
					if(!ruleExists(ruleName)) {
						console.error('Rule with name "'+ ruleName +'" is not found!');
						continue;
					}
					if(config[prop].indexOf(ruleName) !== -1) continue;
					config[prop].push(obj[prop][i]);
				}
			}
			if(typeof obj[prop] === 'string') {
				ruleExpr = obj[prop].split(argSeparator);
				ruleName = ruleExpr[0];
				ruleArgs = ruleExpr[1];
				if(!ruleExists(ruleName) && ruleName !== 'required') {
					console.error('Rule with name "'+ ruleName +'" is not found!');
					continue;
				}
				if(config[prop].indexOf(ruleName) !== -1) continue;
				config[prop].push(obj[prop]);
			}
		}
	}
	function clearConfig() {
		config = {};
	}
	function getValidationMessages() {
		return messages;
	}

	return {
		add: addRule,
		bind: bindRule,
		clear: clearConfig,
		validate: validate,
		hasErrors: hasErrors,
		getValidationMessages: getValidationMessages
	};
}());