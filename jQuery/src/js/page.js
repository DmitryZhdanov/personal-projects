;$(function(){
	'use strict';
	$.fn.serializeObject = function() {
		var o = {},
			a = this.serializeArray();

		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			}
			else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	Number.prototype.formatMoney = function(curr) {
		var currency = currencyInfo[curr],
			n = this,
			c = currency.precision || 2,
			d = currency.pSeparator || '.',
			t = currency.separator || ',',
			s = n < 0 ? '-' : '',
			i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
			j = (j = i.length) > 3 ? j % 3 : 0;

		return currency.sign + s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
	};

	String.prototype.format = function() {
		var args = arguments;

		return this.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] != 'undefined'
				? args[number]
				: match;
		});
	};

	var currencyInfo = {
			'USD': {
				sign: '$',
				precision: 2,
				pSeparator: '.',
				separator: ',',
				regexp: /\d+(?:\,\d+)*(?:\.\d+)?/,
				parse: function(str) {
					var parsed = this.regexp.exec(str);

					return parsed ? parseFloat(parsed.toString().replace(new RegExp(this.separator, 'gi'), '')) : undefined;
				}
			}
		},
		locationInfo = {
			Russia: ['Saratov', 'Moscow', 'Saint Petersburg'],
			USA: ['Chicago', 'Houston', 'Austin'],
			Japan: ['Tokyo', 'Osaka']
		},
		storage = [],
		$tableContent = $('.table > tbody'),
		$tableFooter = $('.table > tfoot'),
		tableTemplate = _.template($('#list-template').html()),
		tableItemTemplate = _.template($('#list-item-template').html()),
		tableMessageTemplate = _.template($('#table-message-template').html()),
		editModalTemplate = _.template($('#edit-modal').html()),
		showModalTemplate = _.template($('#show-modal').html()),
		Product = function(name, email, count, price, delivery, country, city) {
			this.id = generateId();
			this.name = name;
			this.email = email;
			this.count = count;
			this.price = price;
			this.delivery = delivery;
			this.country = country;
			this.city = city;
		};

	function generateId() {
		return Math.floor((1 + Math.random()) * 0x10000);
	}

	// ================
	// Validator setup
	// ================
	validator.add('isNonEmpty',
		function (value) {
			return value && value.toString().trim().length !== 0;
		}, 'Required field');

	validator.add('maxLength',
		function (value, max) {
			return value.toString().trim().length <= max;
		}, 'Length should be less then {0}');

	validator.add('isPositiveInteger',
		function (value) {
			return value && (/^(\d)*$/).test(value);
		}, 'Value must be a number.');

	validator.add('isNumber',
		function (value) {
			return value && (/^(\d)*(\.\d*)?$/).test(value);
		}, 'Value must be a number.');

	validator.add('isEmail',
		function(value) {
			var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			return reg.test(value);
		}, 'Incorrect email');

	// ================
	// Validation bind
	// ================
	validator.bind({
		name: ['isNonEmpty', 'maxLength:15'],
		count: 'isPositiveInteger',
		email: 'isEmail',
		price: 'isNumber',
		country: 'required:delivery'
	});

	// ============
	// Render table
	// ============
	function renderTable(list) {
		// var html = [];
		// _.each(list, function(p) {
		// 	var pHtml = tableItemTemplate(p);
		// 	html.push(pHtml);
		// });
		// $tableContent.html(html.join(''));
		if(list.length === 0) {
			$tableFooter.html(tableMessageTemplate({message: 'No results found.'}));
			$tableContent.html('');
			return;
		}
		else {
			$tableFooter.html('');
			$tableContent.html(tableTemplate({
				list: list,
				tableItemTemplate: tableItemTemplate
			}));
		}
	}

	function renderRow(p) {
		if($('tr',$tableContent).length === 0) {
			$tableFooter.html('');
		}
		$tableContent.append(tableItemTemplate(p));
	}

	// =====================
	// Validate single input
	// =====================
	function validateSingle() {
		var $input = $(this),
			$validationMessage = $input.closest('.form-group').find('.help-block'),
			validateInput = {},
			currencyMask = $input.data('currency'),
			value = $input.val(),
			format;

		// Parse formatted value
		if(currencyMask !== undefined) {
			value = currencyInfo[currencyMask].parse(value);
		}
		validateInput[$input.attr('name')] = $input.val();

		if(validator.validate(validateInput)) {
			$validationMessage.text(validator.getValidationMessages()[0].message);
			$input.parent('.form-group').addClass('has-error');
		}
		else {
			$validationMessage.text('');
			$input.parent('.form-group').removeClass('has-error');
			// Display formatted input
			if(currencyMask !== undefined) {
				format = parseFloat($input.val()).formatMoney('USD');
				$input.val(format);
			}
		}
	}


	// Display unformatted value
	function disableFormat() {
		var $el = $(this);

		$el.val(currencyInfo[$el.data('currency')].parse($el.val()));
	};

	function addProduct(e) {
		e.preventDefault();

		// Parsed form data
		var p = e.data.serializeObject(),
			newProduct,
			errors,
			prop,
			message,
			i;

		// Turn off currency display format
		$('[data-currency]', e.data).each(function(e) {
			var $el = $(this),
				cur = $el.data('currency');

			p[$el.attr('name')] = currencyInfo[cur].parse($(this).val());
		});

		newProduct = new Product(p.name, p.email, p.count, p.price, p.delivery, p.country, p.city );

		if(!validator.validate(newProduct)) {
			// Add new
			storage.push(newProduct);
			e.data.append(renderRow(newProduct));
			closeModal();
		}
		else {
			errors = validator.getValidationMessages();
			$('[name='+errors[0].property+']',e.data).trigger('focus');
			for (i = 0; i < errors.length; i++) {
				prop = errors[i].property;
				message = errors[i].message;
				$('[name='+prop+']',e.data).closest('.form-group').addClass('has-error');
				$('[name='+prop+']',e.data).closest('.form-group').find('.help-block').text(message);
			}
		}
	}

	function editProduct(e) {
		e.preventDefault();

		// Parsed form data
		var p = e.data.form.serializeObject(),
			oldProduct,
			newProduct,
			$row,
			errors,
			prop,
			message,
			i;

		// Turn off currency display format
		$('[data-currency]', e.data.form).each(function(e) {
			var $el = $(this),
				cur = $el.data('currency');

			p[$el.attr('name')] = currencyInfo[cur].parse($(this).val());
		});

		oldProduct =_.find(storage, function(o) {
			return o.id === e.data.id;
		});
		$row = $('tr[data-product-id="'+ e.data.id +'"]', $tableContent);

		newProduct = new Product(p.name, p.email, p.count, p.price, p.delivery, p.country, p.city);
		newProduct.id = oldProduct.id;

		if(!validator.validate(newProduct)) {
			storage = _.remove(storage, function(o) {
				return o.id !== e.data.id;
			});
			storage.push(newProduct);

			$row.before(tableItemTemplate(newProduct));
			$row.remove();
			closeModal();
		}
		else {
			errors = validator.getValidationMessages();
			$('[name='+errors[0].property+']',e.data.form).trigger('focus');
			for (i = 0; i < errors.length; i++) {
				prop = errors[i].property;
				message = errors[i].message;
				$('[name='+prop+']',e.data.form).closest('.form-group').addClass('has-error');
				$('[name='+prop+']',e.data.form).closest('.form-group').find('.help-block').text(message);
			}
		}
	};

	// ===============
	// Delete product
	// ===============
	function removeProduct(e) {
		var id = $(this).data('action-id'),
			$row = $tableContent.find('tr[data-product-id="'+ id +'"]');

		$(this).off('click', removeProduct);
		storage = _.remove(storage, function(o) {
			return o.id !== id;
		});
		$row.remove();
		closeModal();
	};

	function deleteConfimation(e) {
		$('#productDelete [data-modal="confirm"]').on('click', removeProduct)
			.data('action-id', $(this).parents('tr').data('product-id'));

		launchModal('#productDelete');
	}

	// =======
	// Sort
	// =======
	$('[data-sort]').on('click', function(e) {
		e.preventDefault();

		var $el = $(this),
			sortBy,
			sortDir;

		$el.toggleClass('sort-desc');
		if($el.hasClass('sort-desc')) {
			$el.data('sort-direction', 'desc');
		}
		else {
			$el.data('sort-direction', 'asc');
		}
		sortBy = $el.data('sort');
		sortDir = $el.data('sort-direction');
		renderTable(_.orderBy(storage,[sortBy],[sortDir]));
	});

	// ===========
	// Search
	// ===========
	$('#search').on('submit', function(e) {
		e.preventDefault();
		var term = $('#searchTerm', this).val().toLowerCase(),
			filtered = _.filter(storage, function(o) {
				return o.name.toLowerCase().indexOf(term) !== -1;
			});

		renderTable(filtered);
	});

	function addHandler(e) {
		var $form;

		launchModal('#popup', editModalTemplate({
			layout: {
				header: 'New product',
				button: 'Add'
			},
			product: new Product(),
			locationInfo: locationInfo
		}));

		$form = $('[name="add-product"]');
		$form.on('focusout', 'input, select', validateSingle);
		$form.on('focusin', '[data-currency]', disableFormat);
		$form.on('click', '[type="submit"]', $form, addProduct);
	};

	function editHandler(e) {
		var $form,
			id = $(this).parents('tr').data('product-id'),
			product = _.find(storage, function(o) {
				return o.id === id;
			}),
			html = editModalTemplate({
				layout: {
					header: 'Edit product',
					button: 'Update'
				},
				product: product,
				locationInfo: locationInfo
			});

		launchModal('#popup', html);

		$form = $('[name="add-product"]');
		$form.on('focusout', 'input', validateSingle);
		$('[name="price"]',$form).trigger('focusout'); // Trigger format validation
		$form.on('focusin', '[data-currency]', disableFormat);
		$form.on('click', '[type="submit"]', {form: $form, id: id}, editProduct);
	};

	function showProduct(e) {
		var id = $(this).closest('tr').data('product-id'),
			product =_.find(storage, function(o) {
				return o.id === id;
			});

		if(product) launchModal('#popup', showModalTemplate({product: product}));
	}

	// ==============
	// Handlers
	// ==============
	$tableContent.on('click', '[data-action="delete"]', deleteConfimation);
	$tableContent.on('click', '[data-action="edit"]', editHandler);
	$tableContent.on('click','[data-action="show"]', showProduct);
	$('#addProduct').on('click', addHandler);

	$(document).delegate('input[data-pattern]', 'paste keypress', function(e) {
		var regex = new RegExp($(this).data('pattern')),
			key = e.type === 'paste' ? e.originalEvent.clipboardData.getData('text') : String.fromCharCode(!event.charCode ? event.which : event.charCode);

		if(!regex.test($(this).val()+key)) {
			e.preventDefault();
			return false;
		}
	});

	$(document).delegate('#selectAll', 'click', function(e) {
		var $el = $(this);

		if($el.is(':checked'))
			$el.closest('.form-group').find('[type="checkbox"]').each(function() {
				$(this).prop('checked', true);
			});
		else
			$el.closest('.form-group').find('[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
			});
	});
	// ================
	// Delivery state
	// ================
	$(document).delegate('[name="country"]', 'change', function(e) {
		var deliveryType = $('[name="delivery"]').val(),
			country = $(this).val();

		if(deliveryType === 'City') {
			$('[data-country]').each(function() {
				if($(this).data('country') !== country) {
					$(this).parent().addClass('hidden');
					$(this).prop('disabled', true);
				}
				else {
					$(this).parent().removeClass('hidden');
					$(this).prop('disabled', false);
					$('[name="city"]', this).each(function() {
						$(this).prop('checked', false);
					});
				}
			});
		}
		else if(deliveryType === 'Country') {
			$('[data-country]').each(function() {
				$(this).parent().addClass('hidden');
				$(this).prop('disabled', true);
			});
		}
		$(window).trigger('resize');
	});

	$(document).delegate('[name="delivery"]', 'change', function(e) {
		var val = $(this).val(),
			country = $('[name="country"]:checked').val(),
			$countries = $('[name="country"]'),
			$city = $('[name="city"]'),
			$citiesBlock = $('[data-country]');

		if(val === 'Country') {
			$countries.eq(0).closest('.form-group').removeClass('hidden');
			$citiesBlock.each(function() {
				$(this).parent().addClass('hidden');
				$(this).prop('disabled', true);
				$($city, this).each(function() {
					$(this).prop('checked', false);
				});
			});
			$(window).trigger('resize');
			return;
		}
		if(val === 'City') {
			$countries.eq(0).closest('.form-group').removeClass('hidden');
			$citiesBlock.each(function() {
				if($(this).data('country') !== country) {
					$(this).parent().addClass('hidden');
					$(this).prop('disabled', true);
				}
				else {
					$(this).parent().removeClass('hidden');
					$(this).prop('disabled', false);
				}
			});
			$(window).trigger('resize');
			return;
		}
		else {
			$countries.eq(0).closest('.form-group').addClass('hidden');
			$countries.each(function() {
				$(this).prop('checked', false);
			});
			$city.each(function() {
				$(this).prop('checked', false);
			});
			$citiesBlock.each(function() {
				$(this).parent().addClass('hidden');
				$(this).prop('disabled', true);
			});
		}
		$(window).trigger('resize');
	});

	// ==========
	// Modals
	// ==========
	$('.overlay').on('click', function () {
		$(this).hide();
		$('.popup').hide();
	});

	$('.popup').on('click', '[data-modal="close"]', function (e) {
		e.preventDefault();
		closeModal();
	});

	function closeModal(callback) {
		$('.overlay, .popup').hide();
		if(callback) callback();
	}
	function launchModal(id, body) {
		var $modal = $(id),
			winH = $(document).height(),
			winW = $(window).width();

		$('.overlay').css({'width':winW,'height':winH});
		$('.overlay').fadeTo(400,0.4);

		if(body) {
			$modal.html(body);
		}

		$modal.css('top',  winH/2 - $modal.height() /2);
		$modal.css('left', winW/2-$modal.width()/2);
		$modal.fadeIn(200);
	};

	$(window).resize(function () {
		var $box = $('.popup'),
			winH = $(document).height(),
			winW = $(window).width();

		$('.overlay').css({'width':winW,'height':winH});
		$box.css('top',  winH/2 - $box.height()/2);
		$box.css('left', winW/2 - $box.width()/2);
	});

	// =========
	// Temp data
	// =========
	storage.push(new Product('Product 1', 'supcontact@sup.com', 13, 186, 'Country', 'Russia', '' ));
	storage.push(new Product('Product 2', 'supcontact@sup.com', 9, 22.5, 'City', 'Russia', 'Saratov' ));
	storage.push(new Product('Product 3', 'supcontact@sup.com', 6, 90.90, 'Country', 'Japan', '' ));
	storage.push(new Product('Product 4', 'supcontact@sup.com', 2, 110, 'City', 'USA', ['Chicago','Houston'] ));

	renderTable(storage);
}());