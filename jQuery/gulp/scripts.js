(function () {
	'use strict';

	var path = require('path');
	var gulp = require('gulp');
	var conf = require('./conf');
	var browserSync = require('browser-sync');
	var reload      = browserSync.reload;

	var $ = require('gulp-load-plugins')();

	gulp.task('scripts', function() {
	  return gulp.src([path.join(conf.paths.src, '/js/**/*.js'), '!'+path.join(conf.paths.src, '/js/vendor/**/*.js')])
	    .pipe($.eslint())
	    .pipe($.eslint.format())
	    .pipe($.size())
    	.pipe(reload({stream:true}));
	});

	gulp.task('wscripts', ['scripts'], function() {
	  gulp.watch(path.join(conf.paths.src, '/js/**/*.js'), function() {
	    gulp.start('scripts');
	  });
	});

}());