(function () {
	'use strict';

    var gulp = require('gulp');
	var path = require('path');
	var conf = require('./conf');
	var browserSync = require('browser-sync');

	gulp.task('serve', function() {
    	browserSync.init({
	        server: {
	            baseDir: conf.paths.src
	        }
	    });

	    browserSync.watch(
	    	path.join(conf.paths.src, '/**/*.{html}')
		// , {ignored: '*.map.css'}
		).on('change', browserSync.reload);
    });

}());
    