﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.Entities;

namespace UsersAwardsApp.DAL.EF
{
    public class DataContext : DbContext
    {
        public DbSet<UserProfile> Users { get; set; }
        public DbSet<Award> Awards { get; set; }
        public DbSet<Image> Images { get; set; }

        static DataContext()
        {
            Database.SetInitializer(new ContextInitializer());

        }
        public DataContext(string connectionString) 
            :base("DefaultConnection")
        {


        }


        //Fluent Api
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            modelBuilder.Entity<UserProfile>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<UserProfile>()
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<UserProfile>()
                .HasOptional(x => x.Photo);
            modelBuilder.Entity<UserProfile>()
                .HasMany(x => x.Awards)
                .WithMany(x => x.Users);


            modelBuilder.Entity<Award>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<Award>()
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Award>()
                .HasRequired(x => x.Image);

            modelBuilder.Entity<Image>()
                .HasKey(x => x.Id);



            base.OnModelCreating(modelBuilder);
        }
    }

    public class ContextInitializer : DropCreateDatabaseIfModelChanges<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            context.Users.Add(new UserProfile() { Name = "Andrey Ivanov", BirthDate = new DateTime(1991, 10, 26)});
            context.Users.Add(new UserProfile() { Name = "Ivan Andreev", BirthDate = new DateTime(1992, 12, 11)});
            context.Users.Add(new UserProfile() { Name = "Oleg Tulenev", BirthDate = new DateTime(1988, 5, 11)});
            context.Users.Add(new UserProfile() { Name = "Nikolai Dada", BirthDate = new DateTime(1985, 2, 12)});
            base.Seed(context);
        }
    }
}
