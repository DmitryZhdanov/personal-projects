﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.DAL.EF;
using UsersAwardsApp.Entities;
using UsersAwardsApp.DALContracts;

namespace UsersAwardsApp.DAL.Repo
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private DataContext db;
        private UserRepository userRepository;
        private AwardRepository awardRepository;

        public EFUnitOfWork(string connectionString)
        {
            db = new DataContext(connectionString);
        }


        public IAwardRepository<Award> Awards
        {
            get
            {
                if (awardRepository == null)
                    awardRepository = new AwardRepository(db);
                return awardRepository; 
            }
        }


        public IUserRepository<UserProfile> Users
        {
            get
            {
                if(userRepository == null)
                    userRepository = new UserRepository(db);
                return userRepository;
            }
        }
        public void Save()
        {
            db.SaveChanges();
        }

        #region Disposing Helper

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
