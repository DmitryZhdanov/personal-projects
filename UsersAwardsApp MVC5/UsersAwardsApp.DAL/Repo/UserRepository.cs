﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UsersAwardsApp.DAL.EF;
using UsersAwardsApp.Entities;
using UsersAwardsApp.DALContracts;

namespace UsersAwardsApp.DAL.Repo
{
    public class UserRepository : IUserRepository<UserProfile>
    {
        private DataContext db;
        public UserRepository(DataContext context)
        {
            this.db = context;
        }

        public int Create(UserProfile user)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(user.Name) || user.Name.Length > 50)
                    throw new ArgumentException("Name");

                db.Users.Add(user);
                db.SaveChanges();
                return user.Id;
            }catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public bool Delete(int id)
        {
            try
            {
                UserProfile _u = db.Users.SingleOrDefault(x => x.Id == id);
                if (_u != null)
                    db.Users.Remove(_u);

                db.SaveChanges();
                return true;
            }catch
            {
                return false;
            }
        }

        public IEnumerable<UserProfile> Find(Func<UserProfile, bool> predicate)
        {
            foreach (UserProfile item in db.Users.Where(predicate).ToList())
            {
                yield return item;
            }
        }

        public UserProfile Get(int id)
        {
            return db.Users.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<UserProfile> GetAll()
        {
            foreach (UserProfile item in db.Users.ToList())
            {
                yield return item;
            }
        }

        public Image GetImage(int id)
        {
            UserProfile _user = db.Users.SingleOrDefault(x => x.Id == id);
            return _user.Photo != null ? _user.Photo : null;

        }

        public void RemoveImage(int imageId)
        {
            Image photo = db.Images.SingleOrDefault(x => x.Id == imageId);
            db.Images.Remove(photo);
        }

        public bool Update(UserProfile item)
        {
            try
            {
                UserProfile _user = db.Users.SingleOrDefault(x => x.Id == item.Id);
                Image photo = _user.Photo;
                if (item.Photo != null)
                {
                    if (photo != null)
                    {
                        photo.ImageData = item.Photo.ImageData;
                        photo.Mime = item.Photo.Mime;
                        db.Entry(photo).State = EntityState.Modified;
                    }
                    else
                    {
                        _user.Photo = item.Photo;
                        db.Entry(_user).State = EntityState.Modified;
                    }
                }

                _user.Name = item.Name;
                _user.BirthDate = item.BirthDate;
                db.Entry(_user).State = EntityState.Modified;

                db.SaveChanges();
                return true;
            }catch
            {
                return false;
            }
        }
    }
}
