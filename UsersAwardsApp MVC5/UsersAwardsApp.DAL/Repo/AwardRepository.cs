﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.DAL.EF;
using UsersAwardsApp.Entities;
using UsersAwardsApp.DALContracts;

namespace UsersAwardsApp.DAL.Repo
{
    public class AwardRepository : IAwardRepository<Award>
    {
        private DataContext db;
        public AwardRepository(DataContext context)
        {
            this.db = context;
        }

        public int Create(Award entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                db.Awards.Add(entity);
                db.SaveChanges();
                return entity.Id;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Award _a = db.Awards.SingleOrDefault(x => x.Id == id);
                if (_a == null)
                    return false;

                db.Awards.Remove(_a);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Award> Find(Func<Award, bool> predicate)
        {
            foreach (Award item in db.Awards.Where(predicate).ToList())
            {
                yield return item;
            }
        }

        public Award Get(int id)
        {
            return db.Awards.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Award> GetAll()
        {
            foreach (Award item in db.Awards.ToList())
            {
                yield return item;
            }
        }

        public Image GetImage(int id)
        {
            Award aw = db.Awards.SingleOrDefault(x => x.Id == id);
            return (aw != null) ? aw.Image : null;
        }

        public bool Update(Award item)
        {
            try
            {
                Award aw = db.Awards.SingleOrDefault(x => x.Id == item.Id);
                if(item.Image != null)
                {
                    aw.Image = item.Image;
                }
                aw.Title = item.Title;
                aw.Description = item.Description;
                db.Entry(aw).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


    }
}
