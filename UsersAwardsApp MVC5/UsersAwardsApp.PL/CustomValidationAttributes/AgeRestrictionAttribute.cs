﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UsersAwardsApp.PL.CustomValidationAttributes
{
    public class AgeRestrictionAttribute : ValidationAttribute
    {
        public AgeRestrictionAttribute(int MinAge, int MaxAge, string ErrorMessage)
        {
            this.MinAge = MinAge;
            this.MaxAge = MaxAge;
            this.ErrorMessage = ErrorMessage;
        }
        public AgeRestrictionAttribute(int MinAge, int MaxAge)
        {
            this.MinAge = MinAge;
            this.MaxAge = MaxAge;
        }

        public int MinAge { get; private set; }
        public int MaxAge { get; private set; }


        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value==null)
                return new ValidationResult(string.Format(ErrorMessage, MinAge, MaxAge));

            DateTime Today = DateTime.Now;
            DateTime Value = (DateTime)value;
            int Age = Today.Year - Value.Year;
            if (Value > Today.AddYears(-Age)) Age--;

            if(Age > MaxAge || Age < MinAge)
            {
                return new ValidationResult(string.Format(ErrorMessage, MinAge, MaxAge));
            }

            return null;
        }
    }
}