﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UsersAwardsApp.PL.Models
{
    public class AwardViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title of award is required")]
        [StringLength(50, ErrorMessage = "Award title should be less than 50 characters")]
        [RegularExpression(@"[a-zA-Z0-9\s-]+", ErrorMessage = "You can use only latin characters for award title")]
        public string Title { get; set; }

        [StringLength(250,ErrorMessage = "Award description should be less than 250 characters")]
        [DisplayFormat(NullDisplayText = "No description for this award")]
        public string Description { get; set; }

        public ImageViewModel Image { get; set; }
        public int UsersCount { get; set; }
    }
}