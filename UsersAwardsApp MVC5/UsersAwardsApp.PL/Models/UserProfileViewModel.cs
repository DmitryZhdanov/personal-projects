﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UsersAwardsApp.PL.CustomValidationAttributes;

namespace UsersAwardsApp.PL.Models
{
    public class UserProfileViewModel
    {
        public int Id { get; set; }

        [Display(Name ="User Name")]
        [Required(ErrorMessage ="Name is required")]
        [StringLength(50,ErrorMessage = "Name length should be less than 50 characters")]
        [RegularExpression(@"^[a-zA-Zа-яА-ЯёЁ\s_0-9-]+$", ErrorMessage = "User name should contain only letters, numbers and white spaces.")]
        public string Name { get; set; }

        [Display(Name = "Birth Date")]
        [Required(ErrorMessage = "Birth date is required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",ApplyFormatInEditMode = true)]
        [AgeRestriction(0,150, "Your age must be less than {1}")]
        public DateTime BirthDate { get; set; }

        public ImageViewModel Photo { get; set; }

        public int Age
        {
            get
            {
                var today = DateTime.Today;
                var age = today.Year - BirthDate.Year;
                if (BirthDate > today.AddYears(-age)) age--;

                return age;
            }
        }
    }
}