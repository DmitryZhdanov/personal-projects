﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UsersAwardsApp.PL.CustomValidationAttributes;

namespace UsersAwardsApp.PL.Models.WebApi
{
    public class UserProfileApiModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [RegularExpression(@"[\wа-яА-ЯёЁ\s]+")]
        public string Name { get; set; }

        [Required]
        [AgeRestriction(0, 150)]
        public DateTime BirthDate { get; set; }

        public int AwardsCount { get; set; }

        public int Age
        {
            get
            {
                var today = DateTime.Today;
                var age = today.Year - BirthDate.Year;
                if (BirthDate > today.AddYears(-age)) age--;

                return age;
            }
        }
    }
}