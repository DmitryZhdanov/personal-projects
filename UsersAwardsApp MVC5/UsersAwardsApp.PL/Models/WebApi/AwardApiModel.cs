﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UsersAwardsApp.PL.CustomValidationAttributes;

namespace UsersAwardsApp.PL.Models.WebApi
{
    public class AwardApiModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [RegularExpression(@"[a-zA-Z0-9\s-]+")]
        public string Title { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public int UsersCount { get; set; }
    }
}