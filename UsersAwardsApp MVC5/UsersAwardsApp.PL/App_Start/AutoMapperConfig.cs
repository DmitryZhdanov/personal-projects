﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UsersAwardsApp.Entities;
using UsersAwardsApp.PL.Models;
using UsersAwardsApp.PL.Models.WebApi;

namespace UsersAwardsApp
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(
                cfg =>
                {
                    cfg.AddProfile<AutoMapperPresentationProfile>();
                });

            Mapper.AssertConfigurationIsValid();
        }
    }

    public class AutoMapperPresentationProfile : Profile
    {
        public AutoMapperPresentationProfile()
        {
            CreateMap<UserProfileViewModel, UserProfile>()
                .ForMember(dest => dest.Photo, c => c.ResolveUsing(x => x.Photo))
                .ForMember(dest => dest.Awards, c => c.Ignore());

            CreateMap<UserProfileViewModel, UserProfile>()
                .ForMember(dest => dest.Photo, c => c.ResolveUsing(x => x.Photo))
                .ForMember(dest => dest.Awards, c => c.Ignore())
                .ReverseMap();

            CreateMap<ImageViewModel, Image>();
            CreateMap<ImageViewModel, Image>()
                .ReverseMap();

            CreateMap<AwardViewModel, Award>()
                .ForMember(dest => dest.Image, c => c.ResolveUsing(x => x.Image))
                .ForMember(dest => dest.Users, c => c.Ignore());

            CreateMap<Award, AwardViewModel>()
                .ForMember(dest => dest.Image, c => c.ResolveUsing(x => x.Image));

            // Web Api
            CreateMap<UserProfile, UserProfileApiModel>();
            CreateMap<UserProfileApiModel, UserProfile>()
                .ForMember(dest => dest.Photo, c => c.Ignore())
                .ForMember(dest => dest.Awards, c => c.Ignore());


            CreateMap<Award, AwardApiModel>();
            CreateMap<AwardApiModel, Award>()
                .ForMember(dest => dest.Image, c => c.Ignore())
                .ForMember(dest => dest.Users, c => c.Ignore());



        }
    }
}