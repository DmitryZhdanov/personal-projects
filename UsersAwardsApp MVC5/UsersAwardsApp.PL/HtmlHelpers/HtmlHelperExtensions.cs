﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc.Ajax;

namespace System.Web.Mvc
{
    public static class HtmlHelperExtensions
    {

        public static MvcHtmlString IconAjaxLink(this AjaxHelper helper, string iconClass, string altText, string actionName, object routeValues, object htmlAttributes, AjaxOptions ajaxOptions)
        {
            var builderI = new TagBuilder("i");
            builderI.MergeAttribute("class", iconClass);
            builderI.MergeAttribute("alt", altText);

            var link = helper.ActionLink("[r]", actionName, routeValues, ajaxOptions, htmlAttributes).ToString();
            return new MvcHtmlString(link.Replace("[r]", builderI.ToString()));
        }

        public static MvcHtmlString ImageAjaxLink(this AjaxHelper helper, string iconClass, string altText, string actionName, object routeValues, object htmlAttributes, AjaxOptions ajaxOptions)
        {
            var builderI = new TagBuilder("i");
            builderI.MergeAttribute("class", iconClass);
            builderI.MergeAttribute("alt", altText);

            var link = helper.ActionLink("[r]", actionName, routeValues, ajaxOptions, htmlAttributes).ToString();
            return new MvcHtmlString(link.Replace("[r]", builderI.ToString()));
        }

        public static MvcHtmlString FileFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string css)
        {

            var InputBuilder = new TagBuilder("input");
            InputBuilder.MergeAttribute("type", "file");
            InputBuilder.MergeAttribute("name", ExpressionHelper.GetExpressionText(expression));
            InputBuilder.MergeAttribute("id", ExpressionHelper.GetExpressionText(expression));
            InputBuilder.MergeAttribute("class", css);
            return new MvcHtmlString(InputBuilder.ToString(TagRenderMode.SelfClosing));
        }
    }
}