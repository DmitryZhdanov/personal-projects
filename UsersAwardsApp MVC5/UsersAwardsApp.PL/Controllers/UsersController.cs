﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UsersAwardsApp.PL.Models;
using MimeTypes;
using System.Text;
using UsersAwardsApp.Entities;
using MvcSiteMapProvider;
using UsersAwardsApp.LogicContracts;
using UsersAwardsApp.LogicContracts.DTO;

namespace UsersAwardsApp.PL.Controllers
{
    public class UsersController : BaseController
    {
        public UsersController(IUserService userService, IAwardService awardService)
            :base(userService, awardService)
        {

        }


        public ActionResult Index(string term)
        {
            IEnumerable<UserProfile> userDtos;
            if (!string.IsNullOrWhiteSpace(term))
            {
                SiteMaps.Current.CurrentNode.Description = string.Format("Search by \"{0}\"",term);
                userDtos = userService.FindAllByName(term);
            }
            else
            {
                userDtos = userService.GetUsers();
            }

            var users = Mapper.Map<IEnumerable<UserProfile>, IEnumerable<UserProfileViewModel>>(userDtos);
            return View(users);
        }

        [HttpGet]
        public ActionResult UsersByAward(int id)
        {
            Award _award = awardService.GetAwardById(id);
            if(_award != null)
            {
                SiteMaps.Current.CurrentNode.ParentNode.Title = _award.Title;
                SiteMaps.Current.CurrentNode.Description = string.Format("Users with award \"{0}\"",_award.Title);
                return View("Index", Mapper.Map<IEnumerable<UserProfile>, IEnumerable<UserProfileViewModel>>(userService.GetUsersByAward(id)));
            }else
            {
                return HttpNotFound("Award not found");
            }
        }

        public ActionResult FindByName (string term)
        {
            UserProfile _user = userService.FindByName(term);
            if(_user != null)
            {
                UserProfileViewModel _userView = Mapper.Map<UserProfile, UserProfileViewModel>(_user);
                SiteMaps.Current.CurrentNode.Title = _user.Name;
                return View("UserPage", _userView);
            }
            return new HttpNotFoundResult("User not found");
        }


        public ActionResult UserPage(int? userId)
        {
            if (!userId.HasValue) return HttpNotFound();
            UserProfile _user = userService.GetUserById(userId.Value);
            if (_user != null)
            {
                SiteMaps.Current.CurrentNode.Title = _user.Name;
                return View(Mapper.Map<UserProfile, UserProfileViewModel>(_user));
            }
            else
                return HttpNotFound("User not found.");
        }

        [HttpGet]
        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(UserProfileViewModel model, HttpPostedFileBase file)
        {
            if(file != null)
            {
                string MimeType = file.ContentType;
                if (file != null && !MimeType.Contains("image"))
                    ModelState.AddModelError(nameof(model.Photo), "Images only allowed");
            }
            

            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    byte[] data = new byte[file.ContentLength];
                    file.InputStream.Read(data, 0, data.Length);
                    model.Photo = new ImageViewModel() { ImageData = data, Mime = file.ContentType };
                }

                var user = Mapper.Map<UserProfileViewModel, UserProfile>(model);
                userService.CreateUser(user);
                return RedirectToAction("Index");
            }
            else
                return View(model);

        }
        
        [ActionName("Edit")]
        public ActionResult Edit(int userId)
        {
            UserProfile userDto = userService.GetUserById(userId);
            if (userDto != null)
            {
                ViewBag.HasImage = (userDto.Photo != null) ? true : false;

                SiteMaps.Current.CurrentNode.ParentNode.Title = userDto.Name;
                var user = Mapper.Map<UserProfile, UserProfileViewModel>(userDto);
                return View(user);
            }
            else
                return HttpNotFound("User not found.");
        }
        [HttpPost]
        public ActionResult Edit(UserProfileViewModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    byte[] data = new byte[file.ContentLength];
                    file.InputStream.Read(data, 0, data.Length);
                    model.Photo = new ImageViewModel() { ImageData = data, Mime = file.ContentType };
                }

                if (userService.UpdateUser(Mapper.Map<UserProfileViewModel, UserProfile>(model))) {
                    return RedirectToAction("Index");
                }else
                {
                    return HttpNotFound("Error");
                }
            }
            else
                return View(model);
        }

        public ActionResult RemoveImage(int userId)
        {
            UserProfile userDto = userService.GetUserById(userId);
            if (userDto != null)
            {
                if (userDto.Photo == null)
                    return RedirectToAction("Edit", new { id = userId });

                userService.RemoveImage(userDto.Photo.Id);
                return RedirectToAction("Edit", new { id = userId });
            }
            else
                return HttpNotFound("User not found.");
        }


        [HttpGet]
        public ActionResult DeleteAjax(int userId)
        {
            if (!Request.IsAjaxRequest())
                return RedirectToAction("Delete", Request.RequestContext.RouteData.Values);

            UserProfile userDto = userService.GetUserById(userId);
            if(userDto == null)
                return HttpNotFound();

            if (userService.DeleteUser(userDto.Id)) {
                return Json(true, JsonRequestBehavior.AllowGet);
            }else
            {
                return HttpNotFound();
            }

        }

        [HttpGet]
        [ActionName("Delete")]
        public ActionResult Delete(int userId)
        {
            UserProfile userDto = userService.GetUserById(userId);
            if (userDto != null)
            {
                SiteMaps.Current.CurrentNode.ParentNode.Title = userDto.Name;
                var user = Mapper.Map<UserProfile, UserProfileViewModel>(userDto);
                return View(user);
            }
            else
                return HttpNotFound("User not found.");
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmation(int id)
        {
            UserProfile userDto = userService.GetUserById(id);
            if (userDto == null)
                return HttpNotFound();
            if (userService.DeleteUser(userDto.Id))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return HttpNotFound();
            }
        }

        public ActionResult GetProfileImage(int id, string placeHolder)
        {
            UserProfile userDto = userService.GetUserById(id);
            string DefaultImage = "~/Content/img/no-image.jpg";

            var user = Mapper.Map<UserProfile,UserProfileViewModel>(userDto);

            if (user == null || user.Photo == null)
            {
                if (string.IsNullOrEmpty(placeHolder))
                {
                    
                    string mime = MimeTypeMap.GetMimeType(System.IO.Path.GetExtension(HttpContext.Server.MapPath(DefaultImage)));
                    byte[] data = System.IO.File.ReadAllBytes(HttpContext.Server.MapPath(DefaultImage));
                    return File(data, mime);
                }
                else
                {
                    string mime = MimeTypeMap.GetMimeType(System.IO.Path.GetExtension(placeHolder));
                    byte[] data = System.IO.File.ReadAllBytes(HttpContext.Server.MapPath(placeHolder));
                    return File(data, mime);
                }
            }

            

            return File(user.Photo.ImageData, "image/png");
        }



        public ActionResult DownloadReport()
        {
            ReportFileModel report = userService.GenerateReport();
            return File(report.ReportData, report.MimeType, report.FileName);
        }

    }
}