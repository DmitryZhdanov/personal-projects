﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Script.Serialization;
using UsersAwardsApp.Entities;
using UsersAwardsApp.LogicContracts;
using UsersAwardsApp.PL.Models.WebApi;

namespace UsersAwardsApp.PL.Controllers
{
    public class AwardsApiController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IAwardService _awardService;
        public AwardsApiController(IUserService userService,IAwardService awardService)
        {
            this._userService = userService;
            this._awardService = awardService;
        }



        // GET: api/awards/M
        // GET: api/awards/Medal
        [Route(@"api/awards/{term:regex(" + "(^[a-zA-Z]$)|(^[a-zA-Z_]{2,}$)" + ")}")]
        [HttpGet]
        public IHttpActionResult FindAward(string term)
        {
            IEnumerable<AwardApiModel> _awards = Mapper.Map<IEnumerable<Award>, IEnumerable<AwardApiModel>>(_awardService.FindAllByTitle(term));
            return Json(_awards);
        }

        // GET: api/awards
        [Route("api/awards/")]
        [HttpGet]
        public IHttpActionResult GetAllAwards()
        {
            return Json(Mapper.Map<IEnumerable<Award>, IEnumerable<AwardApiModel>>(_awardService.GetAwards()));
        }

        // GET: api/award/25
        [Route("api/award/{id}")]
        [HttpGet]
        public IHttpActionResult GetAward(int id)
        {
            Award award = _awardService.GetAwardById(id);
            if (award == null)
            {
                return NotFound();
            }
            return Json(Mapper.Map<Award, AwardApiModel>(award));
        }

        // GET: api/award/some_medal
        [Route("api/award/{name:regex(^[A-Za-z_]+$)}")]
        [HttpGet]
        public IHttpActionResult GetAwardByTitle(string name)
        {
            Award _award = _awardService.FindAnyByTitle(name);
            if (_award != null)
            {
                return Json(Mapper.Map<Award, AwardApiModel>(_award));
            }
            else
            {
                return NotFound();
            }
        }


        // GET: api/award/2/users
        [Route("api/award/{id}/users")]
        [ActionName("Get")]
        public IHttpActionResult GetUsersForAward(int id)
        {
            Award awardDto = _awardService.GetAwardById(id);
            if (awardDto == null)
            {
                return NotFound();
            }
            IEnumerable<UserProfile> users = _userService.GetUsersByAward(id);
            return Json(Mapper.Map<IEnumerable<UserProfile>, IEnumerable<UserProfileApiModel>>(users));
        }

        // PUT: api/award/2
        [Route("api/award/{id}")]
        public IHttpActionResult Put(int id, [FromBody]AwardApiModel model)
        {
            var award = _awardService.GetAwardById(id);
            if (award == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                model.Id = id;
                if (_awardService.UpdateAward(Mapper.Map<AwardApiModel, Award>(model)))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Updating Error");
                }
            }
            else
                return BadRequest("Updating Error");

        }

        // DELETE: api/award/2
        [Route("api/award/{id}")]
        public IHttpActionResult Delete(int id)
        {
            if (_awardService.DeleteAward(id))
            {
                return Ok();
            }
            else
            {
                return BadRequest("Can't delete award");
            }
        }
    }
}
