﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using UsersAwardsApp.Entities;
using UsersAwardsApp.LogicContracts;
using UsersAwardsApp.PL.Models.WebApi;

namespace UsersAwardsApp.PL.Controllers
{
    public class UsersApiController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IAwardService _awardService;
        public UsersApiController(IUserService userService,IAwardService awardService)
        {
            this._userService = userService;
            this._awardService = awardService;
        }

        // GET: api/users/G
        // GET: api/users/Goodie
        [Route(@"api/users/{term:regex("+ "(^[a-zA-Z]$)|(^[a-zA-Z_]{2,}$)" + ")}")]
        [HttpGet]
        public IHttpActionResult FindUser(string term)
        {
            IEnumerable<UserProfileApiModel> _users = Mapper.Map<IEnumerable<UserProfile>,IEnumerable<UserProfileApiModel>>(_userService.FindAllByName(term));
            return Json(_users);
        }

        // GET: api/users
        [Route("api/users/")]
        public IHttpActionResult Get()
        {
            return Json(Mapper.Map<IEnumerable<UserProfile>, IEnumerable<UserProfileApiModel>>(_userService.GetUsers()));
        }

        // GET: api/user/25
        [Route("api/user/{id}")]
        public IHttpActionResult Get(int id)
        {
            UserProfile user = _userService.GetUserById(id);
            if (user == null)
            {
                return NotFound();
            }
            return Json(Mapper.Map<UserProfile,UserProfileApiModel>(user));
        }

        // GET: api/user/goodie
        [Route("api/user/{name:regex(^[A-Za-z_]+$)}")]
        public IHttpActionResult Get(string name)
        {
            UserProfile _user = _userService.FindByName(name);
            if(_user != null)
            {
                return Json(Mapper.Map<UserProfile,UserProfileApiModel>(_user));
            }else
            {
                return NotFound();
            }
        }


        [Route("api/users/")]
        public IHttpActionResult Post([FromBody]UserProfileApiModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            if (ModelState.IsValid)
            {
                UserProfile user = Mapper.Map<UserProfileApiModel, UserProfile>(model);
                user.Id = _userService.CreateUser(user);
                return Created($"/api/user/{user.Id}", Mapper.Map<UserProfile,UserProfileApiModel>(user));
            }
            return BadRequest("New User not valid");
        }

        [Route("api/user/{id}")]
        public IHttpActionResult Put(int id, [FromBody]UserProfileApiModel updateUser)
        {
            var user = _userService.GetUserById(id);
            if (user == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                updateUser.Id = id;
                if (_userService.UpdateUser(Mapper.Map<UserProfileApiModel, UserProfile>(updateUser)))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Updating Error");
                }
            }
            else
                return BadRequest("Updating Error");

        }

        [Route("api/user/{id}")]
        public IHttpActionResult Delete(int id)
        {

            if (_userService.DeleteUser(id))
            {
                return Ok();
            }
            else
            {
                return BadRequest("Can't delete user");
            }
        }

        // GET: api/user/2/awards
        [Route("api/user/{id}/awards")]
        [ActionName("Get")]
        public IHttpActionResult GetAwardsForUser(int id)
        {
            UserProfile userDto = _userService.GetUserById(id);
            if (userDto == null)
            {
                return NotFound();
            }
            IEnumerable<Award> awards = _awardService.GetAwardForUser(id);
            return Json(Mapper.Map<IEnumerable<Award>, IEnumerable<AwardApiModel>>(awards));

        }


        // Post: api/user/2/awards/2
        [Route("api/user/{userId}/awards")]
        [HttpPost]
        public IHttpActionResult ApplyAward(int userId, [FromBody] int awardId)
        {
            if (ModelState.IsValid)
            {
                Award _award = _awardService.GetAwardById(awardId);
                UserProfile _user = _userService.GetUserById(userId);
                if (_award != null && _user != null)
                {
                    if (_awardService.ApplyAward(userId, awardId))
                        return Ok();
                    return BadRequest("User already has this award");
                }

                return NotFound();
            }

            return NotFound();
        }

        // Delete: api/user/2/awards/2
        [Route("api/user/{userId}/awards/{awardId}")]
        [HttpDelete]
        public IHttpActionResult CancelAward(int userId, int awardId)
        {
            Award _award = _awardService.GetAwardById(awardId);
            UserProfile _user = _userService.GetUserById(userId);
            if (_award != null && _user != null)
            {
                if (_awardService.CancelAward(userId, awardId))
                    return Ok();
                return BadRequest("User has no award");
            }

            return NotFound();
        }

    }
}
