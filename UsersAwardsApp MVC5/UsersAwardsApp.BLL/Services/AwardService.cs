﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.Entities;
using UsersAwardsApp.DALContracts;
using System.Text.RegularExpressions;
using UsersAwardsApp.LogicContracts;

namespace UsersAwardsApp.BLL.Services
{
    public class AwardService : IAwardService
    {
        IUnitOfWork db { get; set; }
        public AwardService(IUnitOfWork uow)
        {
            db = uow;
        }

        public int CreateAward(Award award)
        {
            try
            {
                string pattern = @"[a-zA-Z0-9\s-]+";
                Regex regex = new Regex(pattern);

                if (award == null)
                    throw new ArgumentNullException(nameof(award));
                if (string.IsNullOrWhiteSpace(award.Title) || award.Title.Length > 50 || !regex.IsMatch(award.Title))
                    throw new ArgumentException("Title");
                if (award.Image == null)
                    throw new ArgumentException("Image");

                return db.Awards.Create(award);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Award GetAwardById(int id)
        {
            var award = db.Awards.Get(id);
            if (award == null)
                return null;
            
            return award;
        }

        public IEnumerable<Award> GetAwards()
        {
            return db.Awards.GetAll().ToList();
        }

        public Image GetAwardImage(int id)
        {
            return db.Awards.GetImage(id);
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public bool DeleteAward(int awardId)
        {
            return db.Awards.Delete(awardId);
        }

        public IEnumerable<Award> GetFreeAwards(int userId)
        {
            UserProfile profile = db.Users.Get(userId);
            if (profile == null)
                throw new ArgumentNullException(nameof(profile));

            IEnumerable<Award> awards = profile.Awards.ToList();

            return db.Awards.GetAll().Except(awards).ToList();
        }

        public bool ApplyAward(int userId, int awardId)
        {
            UserProfile _user = db.Users.Get(userId);
            Award _award = db.Awards.Get(awardId);
            if (_user == null)
                throw new ArgumentNullException(nameof(_user));
            if (_award == null)
                throw new ArgumentNullException(nameof(_award));

            if (_user.Awards.Contains(_award))
                return false;

            _user.Awards.Add(_award);
            db.Save();

            return true;
        }


        public bool CancelAward(int userId, int awardId)
        {
            UserProfile _user = db.Users.Get(userId);
            Award _award = db.Awards.Get(awardId);
            if (_user == null)
                throw new ArgumentNullException(nameof(_user));
            if (_award == null)
                throw new ArgumentNullException(nameof(_award));

            if (!_user.Awards.Contains(_award))
                return false;

            _user.Awards.Remove(_award);
            db.Save();
            return true;
        }

        public IEnumerable<Award> GetAwardForUser(int userId)
        {

            UserProfile profile = db.Users.Get(userId);
            if (profile == null)
                return null;
            
            IEnumerable<Award> awards = profile.Awards.ToList();

            return awards;
        }

        public bool UpdateAward(Award award)
        {
            try
            {
                string pattern = @"[a-zA-Z0-9\s-]+";
                Regex regex = new Regex(pattern);

                if (award == null)
                    throw new ArgumentNullException(nameof(award));
                if (string.IsNullOrWhiteSpace(award.Title) || award.Title.Length > 50 || !regex.IsMatch(award.Title))
                    throw new ArgumentException("Title");
                
                return db.Awards.Update(award);
            }catch
            {
                return false;
            }
        }

        public IEnumerable<Award> FindAllByTitle(string term)
        {
            if (term.Length == 1)
            {
                return db.Awards.Find(x => x.Title.StartsWith(term, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            else if (term.Length > 1)
            {
                return db.Awards.Find(x => x.Title.ToLower().Contains(term.ToLower())).ToList();
            }
            else
            {
                throw new ArgumentException(nameof(term));
            }
        }

        public Award FindAnyByTitle(string term)
        {
            if (string.IsNullOrWhiteSpace(term))
                return null;

            term = term.Replace("_", " ");
            Award _award = db.Awards
                .Find(x => x.Title.Equals(term, StringComparison.InvariantCultureIgnoreCase))
                .FirstOrDefault();

            return _award;
        }
    }
}
