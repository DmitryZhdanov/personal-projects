﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.Entities;
using UsersAwardsApp.DALContracts;
using UsersAwardsApp.LogicContracts;
using UsersAwardsApp.LogicContracts.DTO;

namespace UsersAwardsApp.BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork db { get; set; }
        public UserService(IUnitOfWork uow)
        {
            db = uow;
        }

        public int CreateUser(UserProfile user)
        {
            if (string.IsNullOrWhiteSpace(user.Name) || user.Name.Length > 50)
                throw new ArgumentException("Name");
            if(user.BirthDate == null || GetAge(user.BirthDate) < 0 || GetAge(user.BirthDate) > 150)
                throw new ArgumentException("BirthDate");


            return db.Users.Create(user);
        }
        public bool UpdateUser (UserProfile user)
        {
            if (string.IsNullOrWhiteSpace(user.Name) || user.Name.Length > 50)
                throw new ArgumentException("Name");
            if (user.BirthDate == null || GetAge(user.BirthDate) < 0 || GetAge(user.BirthDate) > 150)
                throw new ArgumentException("BirthDate");

            return db.Users.Update(user);
        }   

        public UserProfile GetUserById(int userId)
        {
            return db.Users.Get(userId);
        }

        public IEnumerable<UserProfile> GetUsers()
        {
            return db.Users.GetAll().ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public bool DeleteUser(int userId)
        {
            UserProfile user = db.Users.Get(userId);
            if (user == null)
                throw new ArgumentException(nameof(userId));
            return db.Users.Delete(user.Id);
        }

        public ReportFileModel GenerateReport()
        {
            ReportFileModel report = new ReportFileModel();
            IEnumerable<UserProfile> users = db.Users.GetAll();
            StringBuilder sb = new StringBuilder();
            

            if (users.Count() > 0)
            {
                foreach (var _user in users)
                {
                    int awardsCount = _user.Awards.Count;
                    sb.AppendFormat("User \"{0}\" {1}.{2}",
                        _user.Name, (awardsCount > 0) ? "has awards: " + string.Join(", ", _user.Awards.Select(x => x.Title).ToArray()) : "has no awards",
                        Environment.NewLine);
                }

                byte[] FileData = Encoding.ASCII.GetBytes(sb.ToString());
                report.ReportData = FileData;
            }
            else
            {
                sb.AppendLine("No one user were found");
                byte[] FileData = Encoding.ASCII.GetBytes(sb.ToString());
                report.ReportData = FileData;
            }

            report.MimeType = "text/plain";
            report.FileName = "report.txt";

            return report;
        }

        public void RemoveImage(int imageId)
        {
            db.Users.RemoveImage(imageId);
            db.Save();
        }

        public IEnumerable<UserProfile> FindAllByName(string term)
        {
            if(term.Length == 1)
            {
                return db.Users.Find(x => x.Name.StartsWith(term, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            else if (term.Length > 1)
            {
                return db.Users.Find(
                    x => x.Name.StartsWith(term, StringComparison.InvariantCultureIgnoreCase) ||
                    x.Name.EndsWith(term, StringComparison.InvariantCultureIgnoreCase)
                    ).ToList();
            }
            return db.Users.GetAll();
        }

        public UserProfile FindByName(string term)
        {
            if (string.IsNullOrWhiteSpace(term))
                return null;

            UserProfile _user = db.Users
                .Find(x => x.Name.Equals(term, StringComparison.InvariantCultureIgnoreCase) ||
                           x.Name.Equals(term.Replace("_"," "), StringComparison.InvariantCultureIgnoreCase))
                .OrderBy(x => x.BirthDate)
                .FirstOrDefault();

            return _user;
        }

        public IEnumerable<UserProfile> GetUsersByAward(int awardId)
        {
            Award _award = db.Awards.Get(awardId);
            if (_award == null)
                throw new ArgumentException(nameof(awardId));

            return _award.Users.ToList();
        }
        private int GetAge(DateTime BirthDate)
        {
            var today = DateTime.Today;
            var age = today.Year - BirthDate.Year;
            if (BirthDate > today.AddYears(-age)) age--;

            return age;
        }
    }
}
