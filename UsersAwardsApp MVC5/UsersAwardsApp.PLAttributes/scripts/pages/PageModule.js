﻿var PageModule = (function () {
	var lastClickedList,
		overlay = $("#modal-background"),
		searchField = "#SearchField",
		searchForm = "#SearchForm",
		clearSearch = "#ClearSearch",
		errorModal = $("#ErrorModal"),
		awardModal = $('#AwardModal');

	String.prototype.isEmpty = function() {
	    return (this.length === 0 || !this.trim());
	};

	var initBase = function(){
			// Block UI settings
			$.blockUI.defaults.blockMsgClass = "block-ui";
			$.blockUI.defaults.message = "<i class='fa fa-spinner spin'></i>";
			$.blockUI.defaults.overlayCSS = {
				backgroundColor: "#fff",
				opacity: 0.5
			};
			
			$('[data-toggle="tooltip"]').tooltip();
    		EmptySearchReset();
		}; 

    var initHandlers = function(){
		// LOAD AWARD MODAL  
    	$(document).delegate("[data-load-award]","click",function(){
			event.preventDefault();
    		var awardId = $(this).data("load-award");
			var ModalLink = $(this).prop("href");

			$.ajax({
				url: ModalLink,
				type: 'get',
				beforeSend: function(){
				    $("body").block();
				},
				success: function(data){
					$('.modal-title',awardModal).text("Award info");
					$('.modal-body',awardModal).html(data);
					awardModal.modal('show');
				},
				error: function(ev,d){
					$(".modal-title",errorModal).text("Error");
					$(".modal-body",errorModal).html(d);
					console.log(d)
					errorModal.modal('show');
				},
				complete: function () {
				    setTimeout(function () {
				        $("body").unblock();
				    },200)
				}
			});
    	});

    	$(document).delegate(searchField,"keyup",function(){
    		EmptySearchReset();
    	});

    	$(document).delegate(clearSearch,"click",function(){
    		var searchVal = $(searchField).val();
    		$(searchField).val("");
    		if(!searchVal.isEmpty())
    			$(searchForm).submit();
    	});

    	///
    	/// Awards list collapse handlers
    	$(document).delegate("[id^=collapseExample]","show.bs.collapse",function(){
			event.preventDefault();
			event.stopPropagation();

			if(!overlay.is(":visible"))
			{
				lastClickedList = this;
				$(lastClickedList).parents(".card-holder").addClass("highlight");
				overlay.show()
				overlay.animate({
				    opacity: 0.2
				  }, 200, function() {

				  });
			}
    	});

    	$(document).delegate("[id^=collapseExample]","hide.bs.collapse",function(){
			event.preventDefault();
			event.stopPropagation();

			overlay.animate({
				    opacity: 0
				  }, 200, function() {
				    overlay.hide();
					overlay.data("highlightedElement",null);
			        $(lastClickedList).parents(".card-holder").removeClass("highlight");	
					$($(lastClickedList).data("target")).off('hidden.bs.collapse', CollapseHandler);
				  });
    	});

		$(document).delegate("#modal-background","click",function(event){
			$(lastClickedList).collapse('hide');
		});
    };

    var EmptySearchReset = function(){
    	var searchVal = $(searchField).val();
    		var reset = $(clearSearch);
    		if($(searchForm).length>0)
    		{
    			if(searchVal !== undefined && !searchVal.isEmpty() && reset.is(":hidden"))
    				reset.show();
	    		else if(searchVal.isEmpty() && reset.is(":visible"))
	    			reset.hide();
    		}
    }

	var CollapseHandler = function(){
			overlay.animate({
				    opacity: 0
				  }, 200, function() {
				    overlay.hide();
					overlay.data("highlightedElement",null);
			        $(lastClickedList).parents(".card-holder").removeClass("highlight");	
					$($(lastClickedList).data("target")).off('hidden.bs.collapse', CollapseHandler);
				  });
	}

    var ajax = {
    	deleteUserBegin: function(r,b,d){
    		$(this).parents(".card-holder").block(); 
    	},
    	userDeleteSuccess: function(result,status,xhr){
    		var btn = $(this);
    		setTimeout(function(){
    			$(this).parents(".card-holder").unblock(); 
    			btn.parents(".card-holder").fadeOut();
    		},1000)
    	}
    }

    initBase();
    initHandlers();

    return {
    	ajaxAction : {
    		userDeleteBegin: ajax.deleteUserBegin,
    		userDeleteSuccess: ajax.userDeleteSuccess
    	}
    }
})();