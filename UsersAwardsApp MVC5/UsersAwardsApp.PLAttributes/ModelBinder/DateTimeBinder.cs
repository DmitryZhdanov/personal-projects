﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UsersAwardsApp.PLAttributes.ModelBinder
{
    public class DateTimeBinder : IModelBinder
    {
        private static CultureInfo _culture;

        static DateTimeBinder()
        {
            _culture = CultureInfo.GetCultureInfo("ru-RU");
        }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            var date = value.ConvertTo(typeof(DateTime), _culture);

            return date;
        }
    }

}