﻿using AutoMapper;
using MimeTypes;
using MvcSiteMapProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UsersAwardsApp.Entities;
using UsersAwardsApp.LogicContracts;
using UsersAwardsApp.PLAttributes.Models;

namespace UsersAwardsApp.PLAttributes.Controllers
{
    [RoutePrefix("awards")]
    [Route("~/award/{awardId:int}/{action}")]
    public class AwardsController : BaseController
    {

        public AwardsController(IAwardService awardService, IUserService userService)
            :base(userService, awardService)
        {

        }

        [Route(@"{term:regex(^[a-zA-Z]{1}$|^[^\d]+[a-zA-Z\s0-9-]+$)?}/")]
        // GET: Awards
        public ActionResult Index(string term)
        {
            IEnumerable<Award> awards;
            if (!string.IsNullOrWhiteSpace(term))
            {
                SiteMaps.Current.CurrentNode.Description = string.Format("Search by \"{0}\"", term);
                awards = awardService.FindAllByTitle(term);
            }
            else
            {
                awards = awardService.GetAwards();
            }

            var usersView = Mapper.Map<IEnumerable<Award>, IEnumerable<AwardViewModel>>(awards);
            return View(usersView);
        }


        [Route(@"~/award/{term:length(1,50):regex(^[a-zA-Z]{1}$|^[^\d]+[a-zA-Z\s0-9-]+$)}")]
        public ActionResult FindByName(string term)
        {
            Award _award = awardService.FindAnyByTitle(term);
            if(_award != null)
            {
                AwardViewModel _awardView = Mapper.Map<Award, AwardViewModel>(_award);
                SiteMaps.Current.CurrentNode.Title = _award.Title;
                return View("AwardInfo", _awardView);
            }else
            {
                return new HttpNotFoundResult("Award not found");
            }
        }

        [Route("~/create-award")]
        [HttpGet]
        public ActionResult CreateAward()
        {
            CreateAwardViewModel model = new CreateAwardViewModel();
            return View(model);
        }

        [Route("~/create-award")]
        [HttpPost]
        public ActionResult CreateAward(CreateAwardViewModel model)
        {
            string MimeType = "";
            if (model.Image == null)
            {
                ModelState.AddModelError(nameof(model.Image), "Images is required");
            }
            else
            {
                MimeType = model.Image.ContentType;
                if (!MimeType.Contains("image"))
                    ModelState.AddModelError(nameof(model.Image), "Images only allowed");
            }

            if (ModelState.IsValid)
            {
                byte[] ImageData = new byte[model.Image.ContentLength];
                model.Image.InputStream.Read(ImageData, 0, ImageData.Length);

                AwardViewModel award = new AwardViewModel()
                {
                    Title = model.Title,
                    Description = model.Description,
                    Image = new ImageViewModel() { ImageData = ImageData, Mime = MimeType }
                    
                };
                awardService.CreateAward(Mapper.Map<AwardViewModel, Award>(award));

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [ActionName("Edit")]
        public ActionResult Edit(int awardId)
        {
            Award awardDto = awardService.GetAwardById(awardId);
            if (awardDto != null)
            {
                SiteMaps.Current.CurrentNode.ParentNode.Title = awardDto.Title;
                return View(Mapper.Map<Award, AwardViewModel>(awardDto));
            }
            else
                return HttpNotFound("Award not found.");
        }
        [HttpPost]
        public ActionResult Edit(AwardViewModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    byte[] data = new byte[file.ContentLength];
                    file.InputStream.Read(data, 0, data.Length);
                    model.Image = new ImageViewModel() { ImageData = data, Mime = file.ContentType };
                }

                if(awardService.UpdateAward(Mapper.Map<AwardViewModel, Award>(model)))
                {

                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Update error");
                    return RedirectToAction("Edit",model);
                }
            }
            else
                return View(model);
        }


        [Route("~/award/{id:int}/award-image")]
        [HttpGet]
        public ActionResult GetAwardImage(int id, string placeHolder)
        {
            var imageDto = awardService.GetAwardImage(id);
            if (imageDto != null)
            {

                ImageViewModel image = Mapper.Map<Image, ImageViewModel>(imageDto);
                return File(image.ImageData, image.Mime);
            }
            else
            {
                if (placeHolder == null) return HttpNotFound();
                string mime = MimeTypeMap.GetMimeType(System.IO.Path.GetExtension(placeHolder));
                byte[] data = System.IO.File.ReadAllBytes(HttpContext.Server.MapPath(placeHolder));
                return File(data, mime);
            }
        }

        [Route("~/user/choose-award/{userId:int}/")]
        [HttpGet]
        public ActionResult ChooseAward(int userId)
        {

            UserProfile userDto = userService.GetUserById(userId);
            if (userDto == null)
                return HttpNotFound();

            UserProfileViewModel userView = Mapper.Map<UserProfile, UserProfileViewModel>(userDto);

            IEnumerable<Award> freeAwardsDto = awardService.GetFreeAwards(userId);

            IEnumerable<AwardViewModel> awardsToShow = Mapper.Map<IEnumerable<Award>, IEnumerable<AwardViewModel>>(freeAwardsDto);

            ViewBag.UserId = userView.Id;
            ViewBag.UserName = userView.Name;

            SiteMaps.Current.CurrentNode.ParentNode.Title = userView.Name;

            return View(awardsToShow);
        }

        [Route("~/award-user/{userId:int}_{awardId:int}")]
        [HttpGet]
        public ActionResult ApplyAward(int userId, int awardId)
        {
            UserProfile _userDto = userService.GetUserById(userId);
            UserProfileViewModel _user = Mapper.Map<UserProfile,UserProfileViewModel>(_userDto);


            if (_user == null)
                return HttpNotFound("User not found");

            Award _awardDto = awardService.GetAwardById(awardId);
            AwardViewModel _award = Mapper.Map<Award,AwardViewModel>(_awardDto);

            if (_award == null)
                return HttpNotFound("Award not found");

            SiteMaps.Current.CurrentNode.ParentNode.Title = _user.Name;
            ViewBag.HasAward = _userDto.Awards.Count(x=>x.Id == _awardDto.Id)>0 ? true : false;


            return View(new GiveAwardViewModel() { User = _user, Award = _award });
        }

        [Route("~/award-user/{userId:int}_{awardId:int}")]
        [HttpPost]
        [ActionName("ApplyAward")]
        public ActionResult ApplyAwardConfrimed(int userId, int awardId)
        {
            awardService.ApplyAward(userId, awardId);
            return RedirectToAction("Index", "Users", null);
        }
        [Route("~/cancel-rewarding/{userId:int}_{awardId:int}")]
        [HttpGet]
        public ActionResult CancelAward(int userId, int awardId)
        {
            UserProfile _userDto = userService.GetUserById(userId);
            UserProfileViewModel _user = Mapper.Map<UserProfile, UserProfileViewModel>(_userDto);


            if (_user == null)
                return HttpNotFound("User not found");

            Award _awardDto = awardService.GetAwardById(awardId);
            AwardViewModel _award = Mapper.Map<Award, AwardViewModel>(_awardDto);

            if (_award == null)
                return HttpNotFound("Award not found");

            SiteMaps.Current.CurrentNode.ParentNode.Title = _user.Name;
            ViewBag.HasAward = _userDto.Awards.Count(x => x.Id == _awardDto.Id) > 0 ? true : false;


            return View(new GiveAwardViewModel() { User = _user, Award = _award });
        }

        [Route("~/cancel-rewarding/{userId:int}_{awardId:int}")]
        [HttpPost]
        [ActionName("CancelAward")]
        public ActionResult CancelRewardConfirmed(int userId, int awardId)
        {
            awardService.CancelAward(userId, awardId);
            return RedirectToAction("Index", "Users", null);
        }

        [Route("~/user/{id:int}/awards")]
        public ActionResult GetAwardsForUser(int id)
        {
            UserProfile _user = userService.GetUserById(id);
            ViewBag.UserId = id;
            return PartialView("_AwardsCardPartial",(_user != null) ? Mapper.Map<IEnumerable<Award>,IEnumerable<AwardViewModel>>(awardService.GetAwardForUser(_user.Id)) : null);
        }

        [Route("~/award/{id:int}")]
        public ActionResult AwardInfo(int id)
        {
            Award _awardDto = awardService.GetAwardById(id);
            if (_awardDto != null)
            {
                SiteMaps.Current.CurrentNode.Title = _awardDto.Title;
                return View(Mapper.Map<Award, AwardViewModel>(_awardDto));
            }
            else
                return HttpNotFound("Award not found");
        }

        [Route("~/award/{awardId:int}/get-modal/{userId:int}")]
        [HttpGet]
        public ActionResult GetAwardModal(int awardId,int userId)
        {
            if (!Request.IsAjaxRequest())
                return RedirectToAction("AwardInfo", new { id = awardId });
            Award _awardDto = awardService.GetAwardById(awardId);
            if (_awardDto != null)
            {
                ViewBag.UserId = userId;
                return PartialView("_AwardModalPartial",Mapper.Map<Award, AwardViewModel>(_awardDto));
            }
            else
                return Json(false);
        }


        [ActionName("Delete")]
        public ActionResult Delete(int awardId)
        {
            Award awardDto = awardService.GetAwardById(awardId);
            if (awardDto != null)
            {
                SiteMaps.Current.CurrentNode.ParentNode.Title = awardDto.Title;
                var award = Mapper.Map<Award, AwardViewModel>(awardDto);
                return View(award);
            }
            else
                return HttpNotFound("Award not found.");
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmation(int awardId)
        {
            if (awardService.DeleteAward(awardId))
                return RedirectToAction("Index");
            else
                return HttpNotFound("Award not found.");
        }
    }
}