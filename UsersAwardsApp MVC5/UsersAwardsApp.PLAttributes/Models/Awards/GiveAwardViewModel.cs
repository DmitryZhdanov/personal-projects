﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UsersAwardsApp.PLAttributes.Models
{
    public class GiveAwardViewModel
    {
        public virtual UserProfileViewModel User { get; set; }
        public virtual AwardViewModel Award { get; set; }
    }
}