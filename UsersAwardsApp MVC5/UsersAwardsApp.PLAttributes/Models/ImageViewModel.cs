﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UsersAwardsApp.PLAttributes.Models
{
    public class ImageViewModel
    {
        public int Id { get; set; }
        public byte[] ImageData { get; set; }
        public string Mime { get; set; }
    }
}