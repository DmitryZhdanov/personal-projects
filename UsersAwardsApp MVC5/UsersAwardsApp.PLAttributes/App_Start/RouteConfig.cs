﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UsersAwardsApp.PLAttributes.Util;

namespace UsersAwardsApp.PLAttributes
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            //routes.MapRoute(
            //    name: "ApplyAward",
            //    url: "award-user/{userId}_{awardId}",
            //    defaults: new { controller = "Awards", action = "ApplyAward" },
            //    constraints: new { userId = @"[0-9]+", awardId = @"[0-9]+" }
            //    );

            //routes.MapRoute(
            //    name: "CancelAward",
            //    url: "cancel-rewarding/{userId}_{awardId}",
            //    defaults: new { controller = "Awards", action = "CancelAward" },
            //    constraints: new { userId = @"[0-9]+", awardId = @"[0-9]+" }
            //    );

            //routes.MapRoute(
            //    name: "ChooseAward",
            //    url: "choose-award/{userId}",
            //    defaults: new { controller = "Awards", action = "ChooseAward" },
            //    constraints: new { userId = @"[0-9]+" }
            //    );

            //routes.MapRoute(
            //    name: "CreateUser",
            //    url: "create-user",
            //    defaults: new { controller = "Users", action = "CreateUser" }
            //);
            //routes.MapRoute(
            //    name: "CreateAward",
            //    url: "create-award",
            //    defaults: new { controller = "Awards", action = "CreateAward" }
            //);


            //routes.MapRoute(
            //    name: "UserRoute",
            //    url: "user/{userId}/{action}",
            //    defaults: new { controller = "Users", action = "UserPage" },
            //    constraints: new { userId = @"[0-9]+", action = new ExcludeConstraint("Index") }
            //    );

            //routes.MapRoute(
            //    name: "UserPageByName",
            //    url: "user/{term}",
            //    defaults: new { controller = "Users", action = "FindByName" }
            //    );

            //routes.MapRoute(
            //    name: "UsersWithAward",
            //    url: "award/{id}/users",
            //    defaults: new { controller = "Users", action = "UsersByAward" }
            //    );

            //routes.MapRoute(
            //    name: "AwardRoute",
            //    url: "award/{id}/{action}",
            //    defaults: new { controller = "Awards", action = "AwardInfo" },
            //    constraints: new { id = @"[0-9]+", action = new ExcludeConstraint("Index") }
            //    );

            //routes.MapRoute(
            //    name: "Report",
            //    url: "download-report",
            //    defaults: new { controller = "Users", action = "DownloadReport" },
            //    constraints: new { action = new ExpectedValuesConstraint("DownloadReport") }
            //    );

            //routes.MapRoute(
            //    name: "AwardPageByName",
            //    url: "award/{term}",
            //    defaults: new { controller = "Awards", action = "FindByName" }
            //    );


            //routes.MapRoute(
            //    name: "BaseRoute",
            //    url: "{controller}",
            //    defaults: new { controller = "Users", action = "Index" }
            //);

            //// Search
            //routes.MapRoute(
            //    name: "UsersSearch",
            //    url: "{controller}/{term}",
            //    defaults: new { controller = "Users", action = "Index", term = UrlParameter.Optional },
            //    constraints: new { term = "(^[a-zA-Z0-9]$)|(^[a-zA-Z0-9_]{2,}$)" }
            //);
            // Base Route
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Users", action = "Index", id = UrlParameter.Optional },
                constraints: new { id = @"[0-9]+" }
            );

        }
    }
}
