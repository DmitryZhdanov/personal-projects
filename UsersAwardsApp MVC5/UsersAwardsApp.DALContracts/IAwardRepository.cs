﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.Entities;

namespace UsersAwardsApp.DALContracts
{
    public interface IAwardRepository <T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        int Create(T item);
        bool Update(T item);
        bool Delete(int id);
        Image GetImage(int id);
    }
}
