﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.Entities;

namespace UsersAwardsApp.DALContracts
{
    public interface IUnitOfWork
    {
        IUserRepository<UserProfile> Users { get; }
        IAwardRepository<Award> Awards { get; }
        void Dispose();
        void Save();
    }
}
