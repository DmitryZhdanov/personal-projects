﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.LogicContracts;
using UsersAwardsApp.DALContracts;
using UsersAwardsApp.DAL.Repo;
using UsersAwardsApp.BLL.Services;

namespace UsersAwardsApp.NinjectConfig
{
    public static class Config
    {
        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        public static void RegisterServices(IKernel kernel)
        {
            kernel
                .Bind<IUnitOfWork>()
                .To<EFUnitOfWork>()
                .WithConstructorArgument("DefaultConnection");
            kernel
                .Bind<IUserService>()
                .To<UserService>();
            kernel
                .Bind<IAwardService>()
                .To<AwardService>();
        }
    }
}
