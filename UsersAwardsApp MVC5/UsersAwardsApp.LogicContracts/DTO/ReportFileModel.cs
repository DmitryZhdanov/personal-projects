﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsersAwardsApp.LogicContracts.DTO
{
    public class ReportFileModel
    {
        public byte[] ReportData { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
    }
}
