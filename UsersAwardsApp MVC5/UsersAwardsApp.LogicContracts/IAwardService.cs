﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.Entities;

namespace UsersAwardsApp.LogicContracts
{
    public interface IAwardService
    {
        int CreateAward(Award award);
        Award GetAwardById(int id);
        bool DeleteAward(int awardId);
        IEnumerable<Award> GetAwards();
        Image GetAwardImage(int id);
        IEnumerable<Award> GetFreeAwards(int userId);
        IEnumerable<Award> GetAwardForUser(int userId);
        bool ApplyAward(int userId, int awardId);
        bool CancelAward(int userId, int awardId);
        bool UpdateAward(Award award);
        IEnumerable<Award> FindAllByTitle(string term);
        Award FindAnyByTitle(string term);
        void Dispose();
    }
}
