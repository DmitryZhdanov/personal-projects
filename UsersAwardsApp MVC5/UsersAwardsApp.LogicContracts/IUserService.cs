﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersAwardsApp.Entities;
using UsersAwardsApp.LogicContracts.DTO;

namespace UsersAwardsApp.LogicContracts
{
    public interface IUserService
    {
        int CreateUser(UserProfile user);
        UserProfile GetUserById(int id);
        bool DeleteUser(int user);
        IEnumerable<UserProfile> GetUsers();
        bool UpdateUser(UserProfile user);
        void RemoveImage(int imageId);
        ReportFileModel GenerateReport();
        IEnumerable<UserProfile> FindAllByName(string term);
        IEnumerable<UserProfile> GetUsersByAward(int awardId);
        UserProfile FindByName(string term);
        void Dispose();
    }
}
