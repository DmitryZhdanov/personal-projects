﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsersAwardsApp.Entities
{
    public class UserProfile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public virtual ICollection<Award> Awards { get; set; }
        public virtual Image Photo { get; set; }
        public UserProfile()
        {
            this.Awards = new List<Award>();
        }
    }
}
