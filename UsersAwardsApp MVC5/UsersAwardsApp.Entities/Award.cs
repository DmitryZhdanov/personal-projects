﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsersAwardsApp.Entities
{
    public class Award
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual Image Image { get; set; }
        public virtual ICollection<UserProfile> Users { get; set; }
        public Award()
        {
            this.Users = new List<UserProfile>();
        }
    }
}
