(function () {
	'use strict';

	angular
		.module('app')
		.controller('breadcrumbsDrtvCtrl', breadcrumbsDrtvCtrl)
		.component('breadcrumbs', {
			restrict: 'E',
			replace: true,
			bindings: {
				breadcrumbs: '<'
			},
			controllerAs: 'vm',
			controller: 'breadcrumbsDrtvCtrl',
			templateUrl: 'app/common/components/breadcrumbs/breadcrumbs.html',
		});

	breadcrumbsDrtvCtrl.$inject = ['breadcrumbsService', '$state', '$scope', 'states', '$rootScope'];
	function breadcrumbsDrtvCtrl(breadcrumbsService, $state, $scope, states, $rootScope) {
		var vm = this;
		var handleUnsub = $scope.$on('$stateChangeSuccess', updateBreadcrumbs);
		vm.breadcrumbs = [];
		vm.stateGo = _stateGo;
		updateBreadcrumbs();

		function updateBreadcrumbs() {
			if ($state.$current.data.breadcrumbs !== '') {
				vm.breadcrumbs = breadcrumbsService.updateBreadcrumbsArray();
			}
		}

		function _stateGo(state) {
			$state.go(states[state].data.stateName);
		}

		$rootScope.$on('$destroy', function () {
			handleUnsub();
		});
	}
} ());
