var Map = (function(){
	var map = {};
		map.size;

	map.getRandomPoint = function() {
		return new Vector(
			this.size.x * Math.random() * Utils.random('lerp'),
			this.size.y * Math.random() * Utils.random('lerp')
		);
	}
	map.expand = function(expand) {
		this.size.x +=expand;
		this.size.y +=expand;
	}
	map.reset = function() {
		this.size = new Vector(2000,2000);
	}
	map.renderMinimap = function(size, position, dotSize) {
		var playerPercent,
			enemyPercent;

		Renderer.save();
		Renderer.translate(
			Camera.viewport.x + Camera.movement.x - Viewport.center.x,
			Camera.viewport.y + Camera.movement.y - Viewport.center.y);

		// Render bg
		Renderer.image(resources.get('minimap.png'),position.x,position.y)
		Renderer.fillStyle('rgb(0,255,80)');
		playerPercent = new Vector(Director.player.position.x / this.size.x, -Director.player.position.y / this.size.y);
		Renderer.fillRect(
			position.x + (size.x / 2) + (size.x * playerPercent.x / 2) - dotSize,
			position.y + (size.y / 2) + (size.y * playerPercent.y / 2 * -1) - dotSize,
			dotSize,
			dotSize
		);
		// Render enemies
		Renderer.fillStyle('rgb(255,0,0)');
		for (var i = 0; i < Director.enemies.length; i++) {
				enemyPercent = new Vector(Director.enemies[i].position.x / this.size.x, -Director.enemies[i].position.y / this.size.y);
				Renderer.fillRect(
				position.x + (size.x / 2) + (size.x * enemyPercent.x / 2) - dotSize,
				position.y + (size.y / 2) + (size.y * enemyPercent.y / 2 * -1) - dotSize,
				dotSize,
				dotSize
			);
		}
		// Render bonuses
		Renderer.fillStyle('rgb(0,0,255)');
		for (var i = 0; i < Director.bonuses.length; i++) {
				enemyPercent = new Vector(Director.bonuses[i].position.x / this.size.x, -Director.bonuses[i].position.y / this.size.y);
				Renderer.fillRect(
				position.x + (size.x / 2) + (size.x * enemyPercent.x / 2) - dotSize,
				position.y + (size.y / 2) + (size.y * enemyPercent.y / 2 * -1) - dotSize,
				dotSize,
				dotSize
			);
		}

		Renderer.restore();
	}

	map.renderRestriction = function() {
		var padding = 2000;
		Renderer.fillStyle('rgba(250,40,40,0.1)');
		// Left
		Renderer.fillRect(
			-this.size.x - Viewport.center.x - padding,
			-this.size.y - Viewport.center.y - padding, 
			Viewport.center.x + padding,
			this.size.y * 2 + Viewport.height + (padding*2)
		); 
		// Right
		Renderer.fillRect(
			this.size.x,
			-this.size.y, 
			Viewport.center.x + padding,
			this.size.y * 2 + Viewport.center.y + padding
		); 

		// Top
		Renderer.fillRect(
			-this.size.x,
			-this.size.y - Viewport.center.y - padding, 
			this.size.x * 2 + Viewport.center.x + padding,
			Viewport.center.y + padding
		); 

		// Bottom
		Renderer.fillRect(
			-this.size.x,
			this.size.y, 
			this.size.x * 2 ,
			Viewport.center.y + padding
		); 

	}

	map.outOfBoundsX = function(x) {
		return x > this.size.x || x < -this.size.x;
	}
	map.outOfBoundsY = function(y) {
		return y > this.size.y || y < -this.size.y;
	}
	return map;
}())