'use strict';

var Vector = function(x,y) {
	this.x = x;
	this.y = y;
}

Vector.prototype = (function(){
	var p = {};
	p.add = function(vector) {
		return new Vector(this.x+vector.x, this.y+vector.y);
	}
	p.substruct = function(vector) {
		return new Vector(this.x-vector.x, this.y-vector.y);
	}
	p.length = function() {
		return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2));
	}
	p.normalize = function() {
		return {
			x: this.x / this.length(),
			y: this.y / this.length()
		}
	}
	p.angleTo = function(vector) {
		return Math.atan2(vector.y - this.y, vector.x - this.x) * 180 / Math.PI;
	}
	p.copy = function() {
		return new Vector(this.x,this.y);
	}
	return p;
})()