var Director = (function(Director) {
	'use strict';

	Director.player = undefined;
	Director.enemies = [];
	Director.explosions = [];
	Director.bonuses = [];
	Director.enemiesCount = 0;
	Director.world = {
		speed: 2,
		patterns: [],
		initialized: false,
		initialize: function() {
			if(!this.initialized){
				this.patterns.push(Renderer.pattern(resources.get('BackdropBlackLittleSparkBlack.png'), 'repeat'));
				this.patterns.push(Renderer.pattern(resources.get('Parallax60.png'), 'repeat'));
				this.patterns.push(Renderer.pattern(resources.get('BackdropBlackLittleSparkTransparent.png'), 'repeat'));
				this.initialized = true;
			}
		},
		render: function() {
			var i;

			for (i = 0; i < this.patterns.length; i++) {
				Renderer.translate(i * Camera.parallax.x * this.speed + (Director.deltaTime*i*10), i * Camera.parallax.y * this.speed);
				Renderer.fillStyle(this.patterns[i]);
				Renderer.fillRect(
					Camera.viewport.x - Viewport.center.x - (i * Camera.parallax.x * this.speed) -(Director.deltaTime*i*10),
					Camera.viewport.y - Viewport.center.y - (i * Camera.parallax.y * this.speed),
					Viewport.width, Viewport.height
				);

				Renderer.translate(-i * Camera.parallax.x * this.speed -(Director.deltaTime*i*10), -i*Camera.parallax.y * this.speed);
			}
		}
	};

	// ================
	// Timing
	// ================
	Director.deltaTime = 0,
	Director.timeStep = (1/60);

	// ================
	// Keyboard states
	// ================
	Director.keyboard = {
		control: {
			'37': false, // left
			'39': false, // right
			'38': false, // top
			'40': false, // down
			'16': false, // shift
			'17': false, // ctrl
			'90': false  // z
		},
		switcher: {
			'13': function() {
				console.log('enter is pressed');
			}, // enter
			'27': function() {
				Game.setState('PAUSED');
			}  // esc
		},
		clear: function() {
			for(var item in this.control) {
				this.control[item] = false;
			}
		}
	};

	Director.clearLevel = function() {
		this.explosions = [];
		this.enemies = [];
		this.bonuses = [];
		this.player = undefined;
	}

	Director.lose = function() {
		Map.reset();
		Game.storage.set('level', 0);
		if((Game.storage.set('highScore')||0)< this.player.score)
			Game.storage.set('highScore', this.player.score);
		Game.storage.set('latestScore', 0);
		this.clearLevel();
		Game.setState('GAMEOVER');
	}
	Director.won = function() {
		Game.storage.set('latestScore', this.player.score);
		if((Game.storage.set('highScore')||0)< this.player.score)
			Game.storage.set('highScore', this.player.score);
		this.clearLevel();
		this.startLevel();
		Game.setState('WON');
	}

	Director.create = function(type, count, position) {
		var point,i;
		switch(type) {
			case 'player': {
				this.player = new Player(Viewport.width/2, Viewport.height/2, 100, 134, 'F5S1.png');
				this.player.score = Game.storage.get('latestScore') || 0;
			} break;
			case 'enemy': {
				count = count || 1;
				for (i = 0; i < count; i++) {
					point = Map.getRandomPoint();
					this.enemies.push(new Enemy(point.x, point.y,75,100,'EN1D.png'));
				}
			} break;
			case 'bonus': {
				for (i = 0; i < count; i++) {
					if(Utils.random('chanse', 50)) {
						this.bonuses.push(new Bonus({
							position: position
						}));
					}
				}
			} break;
		}
	};

	Director.hit = function(type,amount,i) {
		switch(type) {
			case 'player': {
				this.player.health -= amount;
				if(this.player.health <= 0){
					// Push call stack to finish current sequence before executing
					setTimeout(function() {
						Director.lose();
					},0)
				}
			} break;
			case 'enemy': {
				this.enemies[i].health -= amount;
				if(this.enemies[i].health < 0) {
					this.kill({
						type: 'enemy',
						id: i
					});
				}
			} break;
		}
	};

	Director.render = function(type) {
		var i;
		switch(type) {
			case 'player': {
				this.player.render();
			} break;
			case 'enemies': {
				for (i = 0; i < this.enemies.length; i++) {
					this.enemies[i].render();
				}
			} break;
			case 'explosions': {
				for (i = 0; i < this.explosions.length; i++) {
					this.explosions[i].render();
				}
			} break;
			case 'bonuses': {
				for (i = 0; i < this.bonuses.length; i++) {
					this.bonuses[i].render();
				}
			}
		}
	};

	// Update logic
	Director.update = function() {
		var bonus,
			i;

		// Player
		this.player.update();

		// ===============================
		// Bonuses
		// ===============================
		for (i = 0; i < this.bonuses.length; i++) {
			bonus = this.bonuses[i];
			if(bonus.position.substruct(this.player.position).length() < 100) {
				this.bonuses.splice(i,1);
				this.player.applyBonus(bonus.type, bonus.value);
			}
		}

		// ===============================
		// Enemies
		// ===============================
		if(this.enemies.length == 0) {
			setTimeout(function() {
				Director.won();
			},0)
			return;
		}
		for (i = 0; i < this.enemies.length; i++) {
			this.enemies[i].logic();
			if(this.player.position.substruct(Director.enemies[i].position).length() < 100) {
				Director.hit('player', 10);
				Director.hit('enemy', 100, i);
			}
		}
	}

	// ===================
	// 	Game loop
	// ===================
	Director.loop = function() {
		Game.render();
		if(!Game.isState('PAUSED') && !Game.isState('WON'))
			this.deltaTime += this.timeStep;

		setTimeout(this.loop.bind(this),1000/60);
	};

	Director.initialize = function() {
		Publisher.make(Player.prototype);
		Publisher.make(Director);
		Publisher.make(Renderer);
		Renderer.on('updateSize', 'resize', Camera);
		Player.prototype.on('newplayer', 'some', Director);

		Game.setState('MENU');
		Director.loop();
		Renderer.updateSize();
		Map.reset();
	}

	Director.startLevel = function() {
		Game.storage.set('level', (Game.storage.get('level') || 0) + 1);
		this.enemiesCount = Math.round(Game.storage.get('level') * 6);
		Renderer.reset();

		Viewport.update();
		Camera.reset();
		this.deltaTime = 0;

		this.create('player');
		this.create('enemy', this.enemiesCount);

		this.world.initialize();
		GUI.initialize();
		this.keyboard.clear();

		// Expand map every level
		Map.expand((Game.storage.get('level')-1)*500);
	}

	Director.dataProvider = function(key) {
		switch(key) {
			case '_LEVEL': {
				return Game.storage.get('level');
			} break;
			case '_SCORE': {
				return this.player.score;
			} break;
			case '_HIGHSCORE': {
				return Game.storage.get('highScore') || 0;
			} break;
			case '_HEALTH': {
				return {
					val: this.player.health <= 0 ? 0 : this.player.health,
					max: 100
				};
			} break;
			case '_COOLDOWN': {
				return {
					val: this.player.lastShot + this.player.cooldown < this.deltaTime ? 0 : -(this.deltaTime - this.player.lastShot - this.player.cooldown),
					max: this.player.cooldown
				};
			} break;
			case '_ENEMIES': {
				return {
					val: this.enemies.length,
					max: this.enemiesCount
				};
			}
		}
	}

	Director.kill = function(options) {
		switch(options.type) {
			case 'enemy': {
				this.player.score+=1;
				this.explosions.push(new Explosion({
					position: this.enemies[options.id].position,
					anchor: this.enemies[options.id].sprite.anchor
				},'ship'));
				this.create('bonus', 1, this.enemies[options.id].position);
				this.enemies.splice(options.id,1);
			} break;
		}
	}

	Director.some = function() {
		console.log('NEW PLAYER CREATED');
	}

	return Director;
}(Director || {}))
