// =========================
// Abstracted render module
// =========================
var Renderer = (function() {
	'use strict';
	var r = {},
		node = undefined,
		context = undefined;


	r.updateSize = function() {
		Viewport.update();
		node.width = Viewport.width;
		node.height = Viewport.height;
		r.fire('updateSize');
	}

	r.save = function() {
		context.save();
	}
	r.restore = function() {
		context.restore();
	};
	r.reset = function() {
		context.setTransform(1, 0, 0, 1, 0, 0);
	}
	r.translate = function(x, y) {
		context.translate(x,y);
	}
	r.rotate = function(angle) {
		context.rotate(angle.toRadians());
	}

	r.fillStyle = function(color) {
		context.fillStyle = color;
	}

	r.fillRect = function(x, y, w, h) {
		context.fillRect(x, y, w, h);
	}
	r.pattern = function(image, repeating) {
		return context.createPattern(image, repeating);
	}
	// ===============
	// Sizing
	// ===============
	r.getTextWidth = function(text) {
		return context.measureText(text).width;
	}
	r.setFont = function(size, font) {
		context.font = '{0}px {1}'.format(size,font);
	}
	r.textAlign = function(align) {
		context.textAlign = align;
	}
	r.text = function(text, x, y) {
		context.fillText(text, x, y);
	}

	r.drawTriangle = function(x,y,h) {
		context.beginPath();
		context.moveTo(x,y);
		context.lineTo(x+h*2,y+h);
		context.lineTo(x+h*2,y-h);
		context.fill();
	}

	r.init = function() {
		node = document.getElementById('canvas');
		context = node.getContext('2d');

		window.onresize = this.updateSize;
	}

	r.image = function(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight) {
		switch(arguments.length) {
			case 3: {
				context.drawImage(image, sx, sy);
			} break;
			case 9: {
				context.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
			} break;
		}
	}

	r.init();
	return r;
}());