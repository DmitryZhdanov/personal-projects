var Menu = (function(menu) {
	'use strict';

	menu.elements = {
		'START GAME': {
			state: true,
			callback: function() {
				Director.startLevel();
				Game.setState('INGAME');
			}
		},
		'HELP': {
			state: false,
			callback: function() {
				Game.setState('HELP');
			}
		}
	};

	menu.execute = function() {
		for (var el = 0; el < Object.keys(this.elements).length; el++) { 
			if(this.elements[Object.keys(this.elements)[el]].state === true) {
				this.elements[Object.keys(this.elements)[el]].callback();
			}
		}
	};

	menu.set = function(num) {
		for (var el = 0; el < Object.keys(this.elements).length; el++) {
			if(num > 0 && this.elements[Object.keys(this.elements)[el]].state === true && el !== Object.keys(this.elements).length -1) {
				this.elements[Object.keys(this.elements)[el]].state = false;
				this.elements[Object.keys(this.elements)[el+1]].state = true;
				break;
			}

			if(num < 0 && this.elements[Object.keys(this.elements)[el]].state === true && el !== 0) {
				this.elements[Object.keys(this.elements)[el]].state = false;
				this.elements[Object.keys(this.elements)[el-1]].state = true;
			}
		}
	};

	return menu;
}(Menu || {}));
