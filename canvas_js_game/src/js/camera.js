var Camera = (function(camera) {
	'use strict';

	camera.movement = new Vector(0, 0);
	camera.viewport = new Vector(0, 0);
	camera.parallax = new Vector(0, 0);
	camera.update = function(movement, parallax, position) {
		this.movement = movement;
		this.parallax = this.parallax.substruct(parallax);
		this.viewport = position;
	};
	camera.render = function() {
		Renderer.translate(this.movement.x,this.movement.y);
	}
	camera.reset = function() {
		this.movement = new Vector(0, 0);
		this.viewport = new Vector(Viewport.center.x, Viewport.center.y);
		this.parallax = new Vector(0, 0);
	}
	camera.resize = function() {
		if(Game.isState('INGAME')){
			this.viewport = new Vector(Viewport.center.x,Viewport.center.y)
			Game.setState('PAUSED');
		}
	}
	return camera;
}(Camera || {}));