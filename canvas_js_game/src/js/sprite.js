// =====================
// Sprite constructor
// =====================
var Sprite = function (options) {
	'use strict';

	// Framing
	var frameIndex = 0,
		tickCount = 0;
	this.ticksPerFrame = options.ticksPerFrame || 0;
	this.numberOfFrames = options.numberOfFrames || 1;

	this.width = options.width;
	this.height = options.height;
	this.image = resources.get(options.image);
	this.position = options.position || new Vector(0,0);
	this.anchor = options.anchor || new Vector(0,0);
	this.rotator = options.rotator || new Vector(0,0);

	this.update = function () {
		tickCount += 1;
		if (tickCount > this.ticksPerFrame) {

			tickCount = 0;

			if (frameIndex < this.numberOfFrames - 1) {
				frameIndex += 1;
			} else {
				if(options.callback !== undefined) options.callback();
				else frameIndex = 0;
			}
		}
	};
	this.render = function (angle) {
		angle = angle || 0;

		Renderer.save();
		Renderer.translate(
			this.position.x + this.width / this.numberOfFrames / 2 + this.rotator.x,
			this.position.y + this.height / 2 + this.rotator.y
		);

		Renderer.rotate(angle);
		Renderer.image(
			this.image,
			frameIndex * this.width / this.numberOfFrames,
			0,
			this.width / this.numberOfFrames,
			this.height,
			-(this.width / this.numberOfFrames / 2)  + this.anchor.x - this.rotator.x,
			-(this.height / 2) + this.anchor.y - this.rotator.y,
			this.width / this.numberOfFrames,
			this.height
		);
		Renderer.restore();
	};
}