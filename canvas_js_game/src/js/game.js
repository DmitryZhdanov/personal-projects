var Game = (function(game){
	'use strict';

	var state = {

		PAUSED: {
			active: false,
			bindHandlers: function() {
				addEventListener('keydown', this.handler);
			},
			handler: function(e) {
				if(e.keyCode === 13) Game.setState('INGAME');
			},
			render: function() {
				var size = 26,
					title = 'GAME PAUSED',
					button = 'RESUME',
					cameraPos = Camera.viewport.add(Camera.movement);

				Renderer.translate(cameraPos.x - Viewport.center.x, cameraPos.y - Viewport.center.y);
				Renderer.fillStyle('#333');
				Renderer.fillRect(0, 0, Viewport.width, Viewport.height);

				Renderer.setFont(26, 'Arial');

				Renderer.fillStyle('#eee');
				Renderer.textAlign('center');
				Renderer.text(
					title,
					Viewport.center.x,
					Viewport.center.y
				);

				Renderer.setFont(16, 'Arial');
				Renderer.text(
					button,
					Viewport.center.x,
					Viewport.center.y + size * 3
				);
				Renderer.fillStyle('rgb(50,255,50)');
				Renderer.drawTriangle(
					Viewport.center.x + Renderer.getTextWidth(button) /1.7 + Math.sin(Director.deltaTime*5)*5,
					Viewport.center.y + size * 3 - 6.5,
					4
				);

			 	Renderer.translate(-cameraPos.x + Viewport.center.x, -cameraPos.y + Viewport.center.y);
			}
		},
		GAMEOVER: {
			active: false,
			bindHandlers: function() {
				addEventListener('keydown', this.handler);
			},
			handler: function(e) {
				if(e.keyCode === 13) {
					Game.setState('MENU');
				}
			},
			render: function() {
				var size = 46,
					title = 'GAME OVER',
				 	button = 'BACK TO MENU',
				 	cameraPos = Camera.viewport.add(Camera.movement);

			 	Renderer.translate(cameraPos.x - Viewport.center.x, cameraPos.y - Viewport.center.y);
				Renderer.fillStyle('#333');
				Renderer.fillRect(0, 0, Viewport.width, Viewport.height);

				Renderer.setFont(size, 'Arial');

				Renderer.fillStyle('#eee');
				Renderer.textAlign('center');
				Renderer.text(
					title,
					Viewport.center.x,
					Viewport.center.y
				);

				Renderer.setFont(16, 'Arial');
				Renderer.text(
					button,
					Viewport.center.x,
					Viewport.center.y + size * 3
				);
				Renderer.fillStyle('rgb(50,255,50)');
				Renderer.drawTriangle(
					Viewport.center.x + Renderer.getTextWidth(button) /1.7 + Math.sin(Director.deltaTime*5)*5,
					Viewport.center.y + size * 3 - 6.5,
					4
				);

			 	Renderer.translate(-cameraPos.x + Viewport.center.x, -cameraPos.y + Viewport.center.y);
			}
		},
		WON: {
			active: false,
			bindHandlers: function() {
				addEventListener('keydown', this.handler);
			},
			handler: function(e) {
				if(e.keyCode === 13) {
					Game.setState('INGAME');
				}
			},
			render: function() {
				var size = 46,
					title = (Game.storage.get('level')-1) +' LEVEL COMPLETE!',
				 	button = 'CONTINUE',
				 	cameraPos = Camera.viewport.add(Camera.movement);

			 	Renderer.translate(cameraPos.x - Viewport.center.x, cameraPos.y - Viewport.center.y);
				Renderer.fillStyle('#333');
				Renderer.fillRect(0, 0, Viewport.width, Viewport.height);

				Renderer.setFont(size, 'Arial');

				Renderer.fillStyle('#eee');
				Renderer.textAlign('center');
				Renderer.text(
					title,
					Viewport.center.x,
					Viewport.center.y
				);

				Renderer.setFont(16, 'Arial');
				Renderer.text(
					button,
					Viewport.center.x,
					Viewport.center.y + size * 3
				);
				Renderer.fillStyle('rgb(50,255,50)');
				Renderer.drawTriangle(
					Viewport.center.x + Renderer.getTextWidth(button) /1.7 + Math.sin(Director.deltaTime*5)*5,
					Viewport.center.y + size * 3 - 6.5,
					4
				);

			 	Renderer.translate(-cameraPos.x + Viewport.center.x, -cameraPos.y + Viewport.center.y);
			}
		},
		MENU: {
			active: false,
			bindHandlers: function() {
				addEventListener('keydown', this.handler);
			},
			handler: function(e) {
				if(e.keyCode === 38) Menu.set(-1);
				if(e.keyCode === 40) Menu.set(1);
				if(e.keyCode === 13) Menu.execute();
			},
			render: function() {
				Renderer.reset();
				var offset = 0,
					width = 0,
					y = Viewport.height/2;
					Renderer.fillStyle('#222');
					Renderer.fillRect(0, 0, Viewport.width, Viewport.height);
					Renderer.setFont(26, 'Arial');
					Renderer.textAlign('center');
					Renderer.fillStyle('#eee');

				for (var text in this.elements) {
					width = Renderer.getTextWidth(text);
					Renderer.text(text, Viewport.center.x, y + offset);
					if(this.elements[text].state) {
						Renderer.fillStyle('rgb(50,255,50)');
						Renderer.drawTriangle(Viewport.width/2 + width /1.7 + Math.sin(Director.deltaTime*5)*5, y + offset-9, 7);
						Renderer.fillStyle('#eee');
					}
					offset+=30;
				}
			}.bind(Menu)
		},
		INGAME: {
			active: false,
			bindHandlers: function() {
				addEventListener('keydown', this.handler);
				addEventListener('keyup', this.handler);
			},
			handler: function(e) {
				switch(e.type) {
					case 'keydown': {
						if(Director.keyboard.control[e.keyCode] !== undefined)
							Director.keyboard.control[e.keyCode] = true;
						// Handle switcher's callbacks
						if(Director.keyboard.switcher[e.keyCode])
							Director.keyboard.switcher[e.keyCode]();

					} break;
					case 'keyup': {
						if(Director.keyboard.control[e.keyCode])
							Director.keyboard.control[e.keyCode] = false;
					} break;
				}
			},
			render: function() {
				Camera.render();
				Director.world.render();

				Director.update();
				Director.render('player');
				Director.render('enemies');
				Director.render('explosions');
				Director.render('bonuses');
				Map.renderRestriction();

				GUI.render();
			}
		},
		HELP: {
			active: false,
			bindHandlers: function() {
				addEventListener('keydown', this.handler);
			},
			handler: function(e) {
				if(e.keyCode === 13) Game.setState('MENU');
			},
			render: function() {
				var offset = 0,
					size = 26,
					keys = ['left, right arrows','rotate ship',
					 		'up, down arrows','acceleration',
					 		'escape','pause',
					 		'shift','speed boost',
					 		'ctrl','slow down',
						 	'Z','shoot'],
				 	button = 'BACK TO MENU';

				Renderer.fillStyle('#333');
				Renderer.fillRect(0, 0, Viewport.width, Viewport.height);

				Renderer.setFont(26, 'Arial');

				for (var i = 0; i < keys.length; i++) {
					if(i%2){
						Renderer.fillStyle('#eee');
						Renderer.textAlign('left');
						Renderer.text(
							keys[i].toUpperCase(), Viewport.center.x,
							Viewport.center.y - offset + ((size*keys.length)/keys.length)
						);
						offset+=size;
					}else {
						Renderer.fillStyle('#bbb');
						Renderer.textAlign('right');
						Renderer.text(
							keys[i].toUpperCase()+':', Viewport.center.x -20,
							Viewport.center.y - offset + ((size*keys.length)/keys.length)
						);
					}
				}

				Renderer.textAlign('center');
				Renderer.fillStyle('#eee');
				Renderer.text(
					button,
					Viewport.center.x,
					Viewport.center.y + size*3
				);
				Renderer.fillStyle('rgb(50,255,50)');
				Renderer.drawTriangle(
					Viewport.center.x + Renderer.getTextWidth(button) /1.7 + Math.sin(Director.deltaTime*5)*5,
					Viewport.center.y + size * 3 - 8.5,
					7
				);
			}
		}
	};
	game.clearHandlers = function(handler) {
		removeEventListener('keydown', handler);
		removeEventListener('keyup', handler);
		removeEventListener('keypress', handler);
	}
	game.setState = function(newState) {
		if(state.hasOwnProperty(newState) && state[newState].active == false)
		{
			// clear previous state handlers
			if(state[game.getState()])
				game.clearHandlers(state[game.getState()].handler);

			for(var i in state)
				state[i].active = false;

			state[newState].active = true;
			state[newState].bindHandlers();
		}
	}
	game.getState = function(){
		for(var i in state){
			 if(state[i].active === true)
			 	return i;
		}
	}
	game.isState = function(check) {
		return state[check].active || false;
	}
	game.render = function() {
		state[game.getState()].render();
	}

	game.storage = {
		data: {}
	};
	game.storage.set = function(name,value) {
		this.data[name] = value;
	}
	game.storage.get = function(name) {
		if(this.data.hasOwnProperty(name))
			return this.data[name];
	}


	return game;
}(Game || {}))