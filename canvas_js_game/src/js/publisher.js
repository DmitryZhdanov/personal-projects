var Publisher = (function() {
	'use strict';
	
	var publisher = {};
	publisher.subscribers = {
		any: []
	};
	// turns an object into a publisher
	publisher.make = function (o) { 
		for (var i in this) {
			o[i] = this[i];
			o.subscribers = [];
		}
	};
	publisher.on = function (type, fn, context) {
		type = type || 'any';
		fn = typeof fn === 'function' ? fn : context[fn];
		if (typeof this.subscribers[type] === 'undefined') {
			this.subscribers[type] = [];
		}
		this.subscribers[type].push({fn: fn, context: context || this});
	};
	publisher.remove = function (type, fn, context) {
		this.visitSubscribers('unsubscribe', type, fn, context);
	};
	publisher.fire = function (type, publication) {
		this.visitSubscribers('publish', type, publication);
	};
	publisher.visitSubscribers = function (action, type, arg, context) {
		var pubtype = type || 'any',
		subscribers = this.subscribers[pubtype],
		i,
		max = subscribers ? subscribers.length : 0;

		for (i = 0; i < max; i += 1) {
			if (action === 'publish') {
				subscribers[i].fn.call(subscribers[i].context, arg);
			} else {
				if (subscribers[i].fn === arg &&
					subscribers[i].context === context) {
					subscribers.splice(i, 1);
				}
			}
		}
	};
	
	return publisher;
}())