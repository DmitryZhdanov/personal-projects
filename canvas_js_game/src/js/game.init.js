;(function(game) {
	'use strict';
	resources.onReady(game.initialize);
	resources.load([
		'coin-animation-sprite-sheet.png',
		'F5S1fire.png',
		'F5S1.png',
		'EN1D.png',
		'BackdropBlackLittleSparkTransparent.png',
		'Parallax60.png',
		'BackdropBlackLittleSparkBlack.png',
		'rsz_bullet_blue.png',
		'rsz_bullet_red.png',
		'exp_small_red.png',
		'exp_big.png',
		'minimap.png',
		'bonus_health.png'
	]);

}(Director));