var Viewport = (function(viewport) {
	'use strict';

	viewport.width = null;
	viewport.height = null;
	viewport.update = function() {
		console.log(window.innerHeight)
		if(window.innerHeight >= 960) {
			this.width = window.innerWidth > 1300 ? 1300 : window.innerWidth;
			this.height = window.innerHeight;
			return;
		}
		this.width = window.innerWidth;
		this.height = window.innerHeight;

	};

	Object.defineProperty(viewport, 'center', {
		configurable: false,
		get: function() {
			return {
				x: this.width / 2,
				y: this.height / 2
			};
		}
	});

	return viewport;
}(Viewport || {}));