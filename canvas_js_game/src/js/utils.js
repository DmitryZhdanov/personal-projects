var Utils = (function() {
	'use strict';

	var utils = {};

	utils.normalizeAngle = function(angle) {
		var orientation = angle % 360;
		if (orientation < 0) orientation += 360;
		return orientation;
	}
	utils.seed = function() {
	  function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		  .toString(16)
		  .substring(1);
	  }
	  return s4() + s4() + s4() + s4() +
		s4() + s4() + s4() + s4();
	}
	utils.random = function(type, option) {
		switch(type) {
			case 'chanse': {
				var arr = [100];
				for (var i = 0; i < 100; i++) {
					arr.push(i > option ? false : true);
				}
				return arr[Math.round(Math.random()*100)];
			} break;
			case 'array': {
				return option[Math.floor(Math.random() * option.length)];
			} break;
			case 'lerp': {
				return [-1,1][Math.round(Math.random())];
			} break;
		}
	}
	// Uses radian default angle (-90 def)
	utils.getQuarter = function(angle) {
		if(angle > 0 && angle <= 90)
			return 2;
		else if(angle <= 180)
			return 3;
		else if(angle <= 270)
			return 4;
		else
			return 1;
	}

	Number.prototype.toRadians = function () {
		return this * Math.PI / 180;
	};

	String.prototype.format = function() {
		var args = arguments;
			return this.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] != 'undefined'
				? args[number]
				: match;
		});
	};

	return utils;
}());