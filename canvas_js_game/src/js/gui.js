'use strict';

var GUI = (function(ui){
	ui.elements = {};

	ui.initialize = function() {
		this.elements['_HEALTH'] = new UIElement('HEALTH', new Vector(100,-80), 'left', 14, 'progress');
		this.elements['_COOLDOWN'] = new UIElement('COOLDOWN', new Vector(110,-50), 'left', 14, 'progress');
		this.elements['_HIGHSCORE'] = new UIElement('HIGH SCORE: ', new Vector(-Viewport.center.x+20, -Viewport.center.y + 45), 'left', 26, 'text');
		this.elements['_SCORE'] = new UIElement('SCORE: ', new Vector(-Viewport.center.x+20, -Viewport.center.y + 80), 'left', 26, 'text');
		this.elements['_LEVEL'] = new UIElement('LEVEL: ', new Vector(-Viewport.center.x+20, -Viewport.center.y + 115), 'left', 26, 'text');
		this.elements['_ENEMIES'] = new UIElement('ENEMIES LEFT', new Vector(Viewport.center.x -20	, -Viewport.center.y + 45), 'right', 26, 'range','{0} OF {1}');
	}

	ui.render = function() {
		for (var i = 0; i < Object.keys(this.elements).length; i++) {
			this.elements[Object.keys(this.elements)[i]].render(Director.dataProvider(Object.keys(this.elements)[i]));
		}
		Map.renderMinimap(new Vector(300,300), new Vector(20, Viewport.height - 320), 5);
	}
	return ui;

}(GUI || {}))

var UIElement = function(text, position, align, size, type, format) {
	this.text = text || '';
	this.position = position || new Vector(0,0);
	this.size = size || 16;
	this.type = type;
	this.align = align || 'left';
	this.format = format || '';
	this.render = function(data) {
		Renderer.fillStyle('#fff');
		Renderer.setFont(this.size, 'Arial');
		Renderer.textAlign(this.align);
		switch(this.type){
			case 'progress': {
				Renderer.text(
					this.text,
					Camera.viewport.x + this.position.x + Camera.movement.x,
					Camera.viewport.y + this.position.y + Camera.movement.y
				);

				Renderer.fillStyle('rgb(255,40,40)');
				Renderer.fillRect(
					Camera.viewport.x + this.position.x + Camera.movement.x,
					Camera.viewport.y + this.position.y + Camera.movement.y +2,
					Renderer.getTextWidth(this.text),
					this.size / 2
				);
				Renderer.fillStyle('rgb(30,150,40)');
				Renderer.fillRect(
					Camera.viewport.x + this.position.x + Camera.movement.x,
					Camera.viewport.y + this.position.y + Camera.movement.y +2,
					Renderer.getTextWidth(this.text) * data.val / data.max,
					this.size / 2
				);
			}break;
			case 'text': {
				Renderer.text(
					this.text,
					Camera.viewport.x + this.position.x + Camera.movement.x,
					Camera.viewport.y + this.position.y + Camera.movement.y
				);
				Renderer.text(
					data,
					Renderer.getTextWidth(this.text) + Camera.viewport.x + this.position.x + Camera.movement.x,
					Camera.viewport.y + this.position.y + Camera.movement.y
				);
			} break;
			case 'range': {
				Renderer.textAlign(this.align);
				Renderer.fillStyle('#eee');
				Renderer.text(this.text,
					Camera.viewport.x + this.position.x + Camera.movement.x,
					Camera.viewport.y + this.position.y + Camera.movement.y
					);
				Renderer.fillStyle('rgb(50,180,60)');
				Renderer.text(format.format(data.val,data.max),
					Camera.viewport.x + this.position.x + Camera.movement.x,
					Camera.viewport.y + this.position.y + Camera.movement.y + 26
					);
			} break;
		}

	};
};