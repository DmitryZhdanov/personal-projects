// ============================
// Base entity for inheritance
// ============================
'use strict';
var GameUnit = function() {
	this.health = 100;
	this.immortal = false;
	this.speed = 5;
	this.position = new Vector(Viewport.width/2, Viewport.height/2); 

	// Velocity
	this.angle = -90; // -90 overrides base angle for sprite with zero rotation 
	this.magnitude = 3; // Rotation velocity

	// Calculate vector velocity
	this.xunits = function(inertia, customSpeed) {
		var speed = customSpeed || this.speed;
		return inertia !== undefined ? 
			Math.cos(this.angle.toRadians()) * speed * inertia : 
			Math.cos(this.angle.toRadians()) * speed;
	};
	this.yunits = function(inertia, customSpeed) {
		var speed = customSpeed || this.speed;
		return inertia !== undefined ? 
			Math.sin(this.angle.toRadians()) * speed * inertia : 
			Math.sin(this.angle.toRadians()) * speed;
	};

	this.isAlive = function() {
		return this.immortal ? this.immortal : ( this.health > 0  ? true : false);
	};
};

var Bonus = function(options) {
	var img,
		value;
	this.type = Utils.random('array', ['health']);
	switch(this.type) {
		case 'health': {
			img = 'bonus_health.png';
			value = 10;
		} break;
	}
	this.value = value;
	this.position = options.position;
	this.sprite = new Sprite({
		width: 32,
		height: 32,
		image: img,
		numberOfFrames: 1,
		ticksPerFrame: 1,
		position: this.position,
		anchor: new Vector(32,32)
	});
	this.render = function() {
		this.sprite.render();
	}
}

var Bullet = function(options) {
	// Inherit base unit
	GameUnit.apply(this, arguments);

	this.sprite = new Sprite({
		width: 1088,
		height: 64,
		image: options.image,
		numberOfFrames: 17,
		ticksPerFrame: 1,
		anchor: options.a,
		rotator: options.r
	});

	// Basic properties
	this.position = options.p;

	this.speed = options.speed || 10;
	this.angle = options.angle || 0;
	this.maxLifeTime = options.lifeTime || 20;
	this.currentLifeTime = 0;


	this.draw = function() {
		this.currentLifeTime++;
		this.position.x += this.xunits();
		this.position.y += this.yunits();
		this.sprite.position = this.position;
		
		this.sprite.update();
		this.sprite.render(this.angle + 90);
	};
	this.explode = function(ref) {
		ref.splice(0,1);
		Director.explosions.push(new Explosion({
			position: this.position,
			anchor: this.sprite.anchor
		},'bullet'));
	}

};

var Explosion = function(options, type) {
	this.id = Utils.seed();
	this.position = options.position;
	this.explode = function() {
		for (var i = 0; i < Director.explosions.length; i++) {
			if(Director.explosions[i].id == this.id){ 
				Director.explosions.splice(i,1);
			}					
		}
	}
	switch(type) {
		case 'bullet':
		{
			this.sprite = new Sprite({
				width: 320,
				height: 32,
				image: 'exp_small_red.png',
				numberOfFrames: 10,
				ticksPerFrame: 1,
				callback: this.explode.bind(this),
				position: options.position,
				anchor: options.anchor.substruct(new Vector(0,-32))
			});
		} break;
		case 'ship':
		{
			this.sprite = new Sprite({
				width: 25856,
				height: 256,
				image: 'exp_big.png',
				numberOfFrames: 101,
				ticksPerFrame: 0.5,
				callback: this.explode.bind(this),
				position: options.position,
				anchor: options.anchor.substruct(new Vector(64,64))
			});
		} break;
	}

	this.render = function() {
		this.sprite.update();
		this.sprite.render();
	}
	
};

var Player = function(x, y, w, h, sprite) {
	//Enforcing new
	if (!(this instanceof Player)) {
		return new Player();
	}

	// Inherit base unit
	GameUnit.apply(this, arguments);

	// Basic properties
	this.position = new Vector(x,y);

	this.width = w;
	this.height = h;
	this.sprite = new Sprite({
		width: w,
		height: h,
		image: sprite,
		numberOfFrames: 1,
		ticksPerFrame: 1
	});

	// Shooting
	this.lastShot = 0;
	this.cooldown = 0.2;
	this.bulletSpeed = 24;
	this.bullets = [];
	this.damage = 25;

	this.magnitude = 2.2;
	this.speed = 5;
	this.speedBoost = 1.6;
	this.speedSlow = 0.4;

	this.score = 0;
	this.extras = { // Additional sprite assets
		'engine': new Sprite({
			width: 1700,
			height: 140,
			image: 'F5S1fire.png',
			numberOfFrames: 17, // 18
			ticksPerFrame: 1,
			anchor: new Vector(0,32),
			rotator: new Vector(0,0)
		})
	};

	var currentSpeed = 0,
		inertia = 0.4,  // Time for inertial movement, viscosity effect
		lastImpulse = {
			linear: 0,
			angular: 0
		},
		velocity = {
			linear: 1,
			angular: 0
		};

	this.updateImpulse = function(velocityType, factor) {
		factor = factor || 1;
		switch(velocityType) {
			case 'FORWARD': {
				velocity.linear = factor;
				lastImpulse.linear = Director.deltaTime;
			} break;
			case 'BACKWARD': {
				velocity.linear = -factor;
				lastImpulse.linear = Director.deltaTime;
			} break;
			case 'LEFT': {
				if(velocity.linear === -factor) // Reverse input if moving backwards
				{
					velocity.angular = factor;
				}else {
					velocity.angular = -factor;
				}
				lastImpulse.angular = Director.deltaTime;
			} break;
			case 'RIGHT': {
				if(velocity.linear === -factor)
				{
					velocity.angular = -factor;
				}else {
					velocity.angular = factor;
				}
				lastImpulse.angular = Director.deltaTime;
			} break;
			case 'RESET': {
				velocity.linear = 0;
				velocity.angular = 0;
			}
		}
		
	}
	this.update = function() {
		var i,j,
			distance,
			curentVel,
			amp;
		if(Director.keyboard.control[90]) this.shoot();
		
		// Listen rotation handlers
		if(Director.keyboard.control[37]) this.updateImpulse('LEFT');
		if(Director.keyboard.control[39]) this.updateImpulse('RIGHT');
		
		// Listen acceleration handlers
		if(Director.keyboard.control[38]) this.updateImpulse('FORWARD');
		if(Director.keyboard.control[40]) this.updateImpulse('BACKWARD');

		if(Director.keyboard.control[16]) currentSpeed = this.speedBoost;
		else if(Director.keyboard.control[17]) currentSpeed = this.speedSlow;
		else currentSpeed = 1;

		// ==========================
		// Linear inertia movement
		// ==========================
		if(velocity.linear !== 0) {
			curentVel = lastImpulse.linear + inertia - Director.deltaTime;
			amp = (curentVel / inertia).toFixed(3);


			// ======================
			// Boundaries of map 
			// ======================
			if(Map.outOfBoundsX(this.position.x)) {
				this.updateImpulse('RESET');
				this.updateImpulse('FORWARD');
				this.updateImpulse('RIGHT', 1 - Utils.normalizeAngle(this.position.angleTo(new Vector(-this.position.x,this.position.y)) - this.angle) / 180 * 2);
			}
			if(Map.outOfBoundsY(this.position.y)) {
				this.updateImpulse('RESET');
				this.updateImpulse('FORWARD');
				this.updateImpulse('RIGHT', 1 - Utils.normalizeAngle(this.position.angleTo(new Vector(this.position.x,-this.position.y)) - this.angle) / 180 * 2);
			}

			// ==========================
			// Triggering camera movement
			// ==========================
			Camera.update(
				new Vector(-this.xunits(amp) * velocity.linear * currentSpeed, -this.yunits(amp) * velocity.linear * currentSpeed),
				new Vector(this.xunits(amp,1) * velocity.linear * currentSpeed, this.yunits(amp,1) * velocity.linear * currentSpeed),
				this.position
			);

			// ========================
			// Break negative amplitude
			// ========================
			if(amp <= 0) {
				velocity.linear = 0;
				return;
			}

			// ======================
			// Update position
			// ======================
			this.position.x += this.xunits(amp) * velocity.linear * currentSpeed;
			this.position.y += this.yunits(amp) * velocity.linear * currentSpeed;
			
		}
		
		// ==========================
		// Angular inertia movement
		// ==========================
		if(velocity.angular !== 0) {
			curentVel = lastImpulse.angular + inertia - Director.deltaTime;
			amp = (curentVel / inertia).toFixed(3);
			if(amp <= 0) { // Break negative inertia
				velocity.angular = 0;
				return;
			}
			this.angle += amp * this.magnitude * velocity.angular;
		}

		// ===============================
		// Bullets collisions of this obj
		// ===============================

		for(i = 0; i < this.bullets.length; i++) {
			if(this.bullets[i].currentLifeTime > this.bullets[i].maxLifeTime) {
				this.bullets[i].explode(this.bullets);
			}
			else {
				for (j = 0; j < Director.enemies.length; j++) {
					distance = this.bullets[i].position.substruct(Director.enemies[j].position).substruct(new Vector(50,84)).length();
					if(distance < 100){
						Director.hit('enemy', this.damage, j);
						this.bullets[i].explode(this.bullets);
						return;
					} 
				}
				this.bullets[i].draw();
			}
		} 

	}

	this.render = function() {
		for(var ex in this.extras) {
			this.extras[ex].position = this.sprite.position;
			this.extras[ex].update();
			this.extras[ex].render(this.angle + 90);
		}

		this.sprite.position.x = this.position.x - this.sprite.width/2;
		this.sprite.position.y = this.position.y - this.sprite.height/2;
		this.sprite.update();
		this.sprite.render(this.angle + 90); // Normalize from radian's default angle
	};

	this.shoot = function() { 
		if(this.lastShot + this.cooldown < Director.deltaTime) {
			this.lastShot = Director.deltaTime;
			this.bullets.push(new Bullet({
				p: this.position.copy(),
				a: new Vector(-32, -64),
				r: new Vector(-32, -32),
				angle: this.angle,
				speed: this.bulletSpeed,
				lifeTime: 60,
				image: 'rsz_bullet_blue.png'
			}));
		}
	}

	this.applyBonus = function(type, val) {
		switch(type) {
			case 'health': {
				if(this.health + val <= 100) {
					this.health += val;
				}
				else {
					this.health = 100;
				}
			} break;
		}
	}

	this.fire('newplayer', this);
};	

	


var Enemy = function(x, y, w, h, sprite) {
	// Enforcing new
	if (!(this instanceof Enemy)) {
		return new Enemy();
	}
	// Inherit base unit
	GameUnit.apply(this, arguments);

	this.position = new Vector(x,y);
	this.sprite = new Sprite({
		width: w,
		height: h,
		image: sprite,
		numberOfFrames: 1,
		ticksPerFrame: 1
	});

	// Shooting
	this.lastShot = 0;
	this.cooldown = 0.8;
	this.bulletSpeed = 14;
	this.bulletLifeTime = 50;
	this.bullets = [];
	this.damage = 6;

	this.speed = 0;
	this.FOV = 50; // Angle of view
	this.visibilityRange = 600; // Distance to player if int angle of view
	this.seed = Math.random(); // To determine different velocity for each enemy
	this.noise = Noise(Math.random()); // Different velocity map for each enemy

	this.speed = 3;

	var searchDelta = 0,
		lastPlayerAngle = 0,
		searchSpeed = 1.6;

	this.logic = function() {	
		var toPlayer = this.position.angleTo(Director.player.position) - this.angle,
			normalized = Math.round(Utils.normalizeAngle(toPlayer)),
			deltaCorrection,
			releaseAngle = 0;
		if(
			(normalized > 360 - this.FOV || normalized < this.FOV) && 
			Director.player.position.substruct(this.position).length() < this.visibilityRange)
		{
			// Fix angle imprecision
			if(normalized > 360 - this.FOV) {
				lastPlayerAngle = -(normalized/360);
				this.angle += lastPlayerAngle;
			}

			if(normalized < this.FOV) {
				lastPlayerAngle = 1-(normalized/360);
				this.angle += lastPlayerAngle;
			}

			searchDelta = 0; 
			this.shoot();

			this.speed = 8 * this.seed;

		} else {
			// Speed up angle restoring 
			if(searchDelta < 1) 
				searchDelta += 0.02 
			else 
				searchDelta = 1;

			deltaCorrection = (1-searchDelta) >= 0 ? (1-searchDelta) : 0;

			// Search
			this.angle += lastPlayerAngle * deltaCorrection + (this.noise.perlin2(Director.deltaTime / 2, - Director.deltaTime / 2) * 8) * searchDelta;
			this.speed = 6 * this.seed;
		}

		// ======================
		// Boundaries of map 
		// ======================
		if(Map.outOfBoundsX(this.position.x)) {
			this.angle += Utils.normalizeAngle(this.position.angleTo(new Vector(0,0)) - this.angle);
		}
		if(Map.outOfBoundsY(this.position.y)) {
			this.angle += Utils.normalizeAngle(this.position.angleTo(new Vector(0,0)) - this.angle);
		}

		
		this.position.x += this.xunits();
		this.position.y += this.yunits();
		
	}
	this.render = function() {
		var distance,
			i;
		this.sprite.position = this.position;
		this.sprite.render(this.angle + 90);

		for(i = 0; i < this.bullets.length; i++) {
			if(this.bullets[i].currentLifeTime > this.bullets[i].maxLifeTime){
				this.bullets[i].explode(this.bullets);
			}
			else
				{	
					distance = this.bullets[i].position.substruct(Director.player.position).add(new Vector(50,67)).length();
					if(distance < 100){
						Director.hit('player', this.damage);
						this.bullets[i].explode(this.bullets);
						return;
					} 

					this.bullets[i].draw();
				}
		} 
	}
	this.shoot = function() { 
		if(this.lastShot + this.cooldown < Director.deltaTime) {
			this.lastShot = Director.deltaTime;

			this.bullets.push(new Bullet({
				p: this.position.copy(),
				a: new Vector(15, 0),
				r: new Vector(15, 20),
				angle: this.angle,
				speed: this.bulletSpeed,
				lifeTime: this.bulletLifeTime,
				image: 'rsz_bullet_red.png'
			}));
		}
	}
}