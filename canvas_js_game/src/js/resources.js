;(function() {
	'use strict';

	var resourceCache = {},
		callback = null,
		copacity = 0,
		loaded = 0,
		assets = {
			image: './assets/images/'
		};

	function load(urlArr) {
		copacity = urlArr.length;
		urlArr.forEach(function(url) {
			_load(url);
		});
	}

	function _load(url) {
		if(resourceCache.hasOwnProperty(url)) {
			return resourceCache[url];
		}
		var img = new Image();
		img.src = assets.image + url;
		img.onload = function() {
			resourceCache[url] = this;
			if (Object.keys(resourceCache).length === copacity) {
				callback();
			}
		};
	}

	function get(url) {
		return resourceCache[url];
	}

	function onReady(func) {
		callback = func;
	}

	window.resources = {
		load: load,
		get: get,
		onReady: onReady
	};
}());