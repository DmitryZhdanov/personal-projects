(function () {
	'use strict';

    var gulp = require('gulp');
	var path = require('path');
	var conf = require('./conf');
	var browserSync = require('browser-sync').create();

	gulp.task('serve', function() {
    	browserSync.init({
	        server: {
	            baseDir: conf.paths.src
	        }
	    });

	    browserSync.watch(
	    	path.join(conf.paths.src, '/**/*.{js,css,html}')
		, {ignored: '*.map.css'}).on('change', browserSync.reload);
    });

}());
    