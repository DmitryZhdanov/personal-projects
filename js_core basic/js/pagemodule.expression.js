//==========================
//	Calculate Expression
//==========================
'use strict';
var PageModule = (function(module) {
	module.calc = function(str) {
		var props = {
				Regex: /([+-/*]?)([/*.]+)?([+-]?([+-]+)?\d+(?:[.]\d*)?)/ig,
				RegexGroups: {
					Op: 1,
					RestrictedOp: 2,
					Number: 3,
					RestrictedUnary: 4
				},
				ErrorMsg: 'Expression not valid'
			},
			matches,
			result = 0,
			number = 0,
			op ='',
			i = 0;

		if(str.indexOf('=') === -1) {
			return props.ErrorMsg;
		}

		// Trim spaces without two-sided numbers to catch template errors
		str = str.replace(/(\d+\s*\d+)|(\s)|(=.*)/g, function(match) {
			return match.trim().length > 0 && match.indexOf('=') == -1 ? match : '';
		});

		while (matches = props.Regex.exec(str)) {
			//Catch "-*5", "---5" etc.
			if(matches[props.RegexGroups.RestrictedOp] || matches[props.RegexGroups.RestrictedUnary]) {
				return props.ErrorMsg;
			}

			number = parseFloat(matches[props.RegexGroups.Number]);
			op = matches[props.RegexGroups.Op];

			// Only first item can be without operator
			if((i!==0 && op.length === 0) || (i==0 && /[*/]/.test(op))) {
				return props.ErrorMsg;
			}

			i++;
			if(typeof number !== 'number' || isNaN(number)) {
				return props.ErrorMsg;
			}

			result = _calcHelper(result,number,op);
		}

		return i > 0 ? result.toFixed(2) : props.ErrorMsg;
	};

	var _calcHelper = function(left,right,op) {
		if(left === undefined && right === undefined) {
			return 0;
		}

		switch(op) {
			case '+':
				return left + right;
			case '-':
				return left - right;
			case '*':
				return left * right;
			case '/':
				return left / right;
			case '=':
				return left;
			default:
				return right;
		}
	};

	return module;
}(PageModule || {}));





