(function(){
	var locale_en = {
		name: 'en-US',
		dayName: {
			full: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
			short: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
		},
		monthName: {
			full: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		}
	};
	
	PageModule.defineLocale(locale_en);
}());