'use strict';
(function() {
	var EventHandler = {
		bind: function(el, ev, fn){
			if(window.addEventListener) {
				el.addEventListener(ev, fn, false);
			}
			else if(window.attachEvent) { 
				el.attachEvent('on' + ev, fn); // IE8-
			} else {
				el['on' + ev] = fn;
			}
		},

		unbind: function(el, ev, fn){
			if(window.removeEventListener){
				el.removeEventListener(ev, fn, false);
			}
			else if(window.detachEvent) {
				el.detachEvent('on' + ev, fn);
			}
			else {
				el['on' + ev] = null;
			}
		},
		stop: function(ev) {
			var e = ev || window.event;

			e.cancelBubble = true;
			if (e.stopPropagation) {
				e.stopPropagation();
			};
		}
	};
	var bindHandlers = function(){
		EventHandler.bind(document.querySelector('#InputForm1'),'submit',function(e) {
			var target = (this !== window) ? this : e.srcElement; //IE8-
			e.preventDefault ? e.preventDefault() : (e.returnValue = false); //IE8-

			target.elements.result.value = PageModule.calc(target.elements.input.value);
		});

		EventHandler.bind(document.querySelector('#InputForm2'),'submit',function(e) {
			var target = (this !== window) ? this : e.srcElement;
			e.preventDefault ? e.preventDefault() : (e.returnValue = false);

			target.elements.result.value = PageModule.trucate(target.elements.input.value);
		});

		EventHandler.bind(document.querySelector('#InputForm3'),'submit',function(e) {
			var target = (this !== window) ? this : e.srcElement,
				DateConstruct;
			e.preventDefault ? e.preventDefault() : (e.returnValue = false);

			// Parse date
			DateConstruct = PageModule.parseDate(target.elements.inputDate.value);

			if(DateConstruct == null) {
				return target.elements.result.value = 'Invalid Date';
			}

			//target.elements.result.value = (DateConstruct).format(target.elements.inputFormat.value);
			// OR function
			target.elements.result.value = PageModule.dateFormatter(target.elements.inputFormat.value, DateConstruct);
		});
	};

	bindHandlers();

	// Change locale via external file
	// Or just here by calling function
	// PageModule.defineLocale(localeObject);
	console.log('Current locale: ' + PageModule.currentLocale);

}());


