//=======================
//	Remove duplicates
//=======================
'use strict';
var PageModule = (function(module) {
	module.trucate = function(input) {
		var words = input ? input.split(/[\s.?,;:!]/) : [];

		words = words.filter(function(item) {
			return item.length > 0;
		});

		return words.length === 0 ? input : (function(output) {
			var duplicateCounter,
				i,
				j;

			for(i = 0; i < input.length; i++) { // Letters
				duplicateCounter = 0;
				for(j = 0; j < words.length; j++) { // Words
					if(words[j].toLowerCase().indexOf(input[i].toLowerCase())>=0) {
						duplicateCounter++;
					}
				}
				if(duplicateCounter>=words.length) {
					output = output.replace(input[i],'');
				}
			}

			return output;
		}(input));
	};

	return module;
}(PageModule || {}));