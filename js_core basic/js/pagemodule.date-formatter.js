//=================
//	Date format
//=================
'use strict';
var PageModule = (function(module) {
	var locale_default = {
		name: 'en-US',
		dayName: {
			full: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
			short: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
		},
		monthName: {
			full: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		}
	};

	Date.prototype.getMonthName = function(short) {
		return short ? Date.CultureInfo.monthName.short[this.getMonth()] : Date.CultureInfo.monthName.full[this.getMonth()];
	};

	Date.prototype.getDayName = function(short) {
		return short ? Date.CultureInfo.dayName.short[this.getDay()] : Date.CultureInfo.dayName.full[this.getDay()];
	};

	Object.defineProperty(module, 'currentLocale', {
		configurable: false,
		get: function() {
			return Date.CultureInfo.name;
		}
	});

	module.defineLocale = function(locale) {
		Date.CultureInfo = locale;
	};

	module.dateFormatter = function(format,date) {
		// If called like extension we'll use current instance
		var self = date instanceof Date? date : (this instanceof Date ? this : new Date()),
			p = function p(s) {
				return s.toString().length === 1 ? '0' + s : s;
			};

		return format ? format.replace(/yy(?:yy)?|d{1,4}|M{1,4}|(h|H|m|s)(\1)?/g, function(format) {
			switch (format) {
				case 'hh':
					return p(self.getHours() < 13 ? self.getHours() : (self.getHours() - 12));
				case 'h':
					return self.getHours() < 13 ? self.getHours() : (self.getHours() - 12);
				case 'HH':
					return p(self.getHours());
				case 'H':
					return self.getHours();
				case 'mm':
					return p(self.getMinutes());
				case 'm':
					return self.getMinutes();
				case 'ss':
					return p(self.getSeconds());
				case 's':
					return self.getSeconds();
				case 'yyyy':
					return self.getFullYear();
				case 'yy':
					return self.getFullYear().toString().substring(2, 4);
				case 'dddd':
					return self.getDayName();
				case 'ddd':
					return self.getDayName(true);
				case 'dd':
					return p(self.getDate());
				case 'd':
					return self.getDate().toString();
				case 'MMMM':
					return self.getMonthName();
				case 'MMM':
					return self.getMonthName(true);
				case 'MM':
					return p((self.getMonth() + 1));
				case 'M':
					return self.getMonth() + 1;
				default:
					return format;
			}
		}) : self.toString();
	};

	// Parse string args into a date object
	module.parseDate = function(input) {
		var DateArgs = [],
			Regex = {
				DatePart: /\d+/g,
				CorrectInput: /^\d{4}([,\s]{1,2})?(\d{1,2}\1?){0,5}$/
			};

		DateArgs = input.match(Regex.DatePart);
		if(input.length > 0 && (DateArgs == null || !Regex.CorrectInput.test(input))) {
			return null;
		}

		if(DateArgs != null && DateArgs.length > 0 && DateArgs[0] !== '') {
			DateArgs.splice(0,0,Date); // Add func link to args
		}

		return new (Function.prototype.bind.apply(Date, DateArgs));
	};

	// Bind date proto with external function
	Date.prototype.format = module.dateFormatter;

	//Define base locale
	Date.CultureInfo = locale_default;

	return module;
}(PageModule || {}));
