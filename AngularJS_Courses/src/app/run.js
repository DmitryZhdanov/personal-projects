(function() {
	'use strict';

	angular
		.module('app')
		.run(run);

	run.$inject = ['$rootScope', '$state', '$stateParams', 'authService', '$httpBackend', '$q', 'settings'];

	function run($rootScope, $state, $stateParams, authService, $httpBackend, $q, settings) {
		var onChangeStart;

		// ============================
		// Verifying access permissions
		// ============================
		onChangeStart = $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
			if(!authService.isAnonymous() && toState.name === settings.loginRoute) {
				event.preventDefault();
				$state.go('shell.courses');
			}
			if(!authService.checkAccess(toState)) {
				// Prevent circular redirect
				if(toState.name !== settings.loginRoute) {
					event.preventDefault();
					$state.go(settings.loginRoute);
				}
				return;
			}
		});

		$rootScope.$on('$destroy', function(){
			onChangeStart();
		});
	}

}());
