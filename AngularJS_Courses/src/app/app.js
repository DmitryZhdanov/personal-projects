(function () {
	'use strict';
	angular
		.module('app', [
			'ngAnimate',
			'ngCookies',
			'ngTouch',
			'ngSanitize',
			'ui.router',
			'ui.bootstrap',
			'ui.mask',
			'ngMessages',
			'ngMockE2E',
			'ngResource',
			'LocalStorageModule',
			'app.common',
			'app.backend',
			'toastr'
		]);
}());
