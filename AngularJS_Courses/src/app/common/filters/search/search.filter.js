(function(){
	'use strict';

	angular
		.module('app.common')
		.filter('search', searchFilter);

	function searchFilter() {
		return FilterFn;
		function FilterFn(source, search, trackBy) {
			var keys = Object.keys(search),
				result = [],
				term,
				filtered,
				parts;

			if(!keys.length) {
				return source;
			}

			_.each(keys, function(propName) {
				if (search.hasOwnProperty(propName)) {
					term = search[propName];
					filtered = [];
					parts = term.split(' ');

					if(!(term.trim().length || propName.length)) {
						return;
					}
					_.each(parts, function(part) {
						filtered = _.concat(filtered, _.filter(source, function(x) {
							var prop = x[propName];
							return prop ? prop.toLowerCase().indexOf(part.toLowerCase()) !== -1 : false;
						}));
					});

					result = _.concat(result,
						keys.length > 1 ? _.differenceBy(filtered, result, trackBy || 'id') : filtered
					);
				}
			});

			return result;
		}
	}

}());
