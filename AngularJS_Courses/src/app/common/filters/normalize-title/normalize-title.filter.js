(function(){
	'use strict';

	angular
		.module('app.common')
		.filter('normalizeTitle', normalizeTitle);

	function normalizeTitle(){
		return function(val){
			return val[0].toUpperCase().concat(val.substr(1));
		};
	}
}());
