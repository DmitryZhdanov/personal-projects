(function(){
	'use strict';

	angular
		.module('app.common')
		.filter('time', timeFilter);

	function timeFilter() {
		return function(val, format) {
			if(angular.isUndefined(val) || val.length === 0) {
				return '';
			}

			format = format || 'h m';

			var decCache = [],
				decCases = [2, 0, 1, 1, 1, 2],
				formatKeys = {
					'h': function(data){
						var splitted = data.toString().split('.')[0];
						if(angular.isUndefined(splitted) || splitted <= 0) {
							return '';
						}
						var result = splitted[0] === '0' && splitted.length > 1 ? splitted[1] : splitted;
						return result + decOfNum(result, [' час', ' часа', ' часов']);
					},
					'hh': function(data){
						return data.toString().length === 1 ? '0' + this.h(data) : this.h(data);
					},
					'm': function(data){
						var splitted = data.toString().split('.')[1];
						if(angular.isUndefined(splitted) || splitted <= 0) {
							return '';
						}
						var result = splitted[0] === '0' && splitted.length > 1 ? splitted[1] : splitted;
						return result + decOfNum(result, [' минута', ' минуты', ' минут']);
					},
					'mm': function(data){
						return data.toString().length === 1 ? '0' + this.h(data) : this.h(data);
					}
				};

			// ==================
			// Return
			// ==================
			return formatter(val, format);

			// ==================
			// Functions
			// ==================
			function decOfNum(number, titles) {
				if(!decCache[number]) {
					decCache[number] = number % 100 > 4 && number % 100 < 20 ? 2 : decCases[Math.min(number % 10, 5)];
				}
				return titles[decCache[number]];
			}
			function convert(time) {
				return Math.floor(time / 60) + '.' + time % 60;
			}

			function formatter(input, form) {
				var time = angular.isNumber(input) ? convert(input) : convert(parseFloat(input, 10));
				return form.replace(/h|hh|m|mm/g, function(match, number) {
					return formatKeys[match](time);
				});
			}
		};
	}
}());
