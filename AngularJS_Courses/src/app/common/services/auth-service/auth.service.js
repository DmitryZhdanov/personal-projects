(function() {
	'use strict';

	angular
		.module('app.common')
		.service('authService', authService);

	authService.$inject = ['$injector', '$q', '$http', 'localStorageService', 'urls'];

	function authService($injector, $q, $http, localStorageService, urls) {
		var observers = [],
			authStorage = 'auth';

		this.subscribe = function(callback) {
			observers.push(callback);
			notify(!this.isAnonymous(), callback);
			return unsubscribe(callback);
		};

		this.checkAccess = function(toState) {
			var allowAnonymous = toState.allowAnonymous || false;
			if(!allowAnonymous && this.isAnonymous()) {
				return false;
			}
			return true;
		};

		this.login = function(credentials) {
			if(!this.isAnonymous()) {
				return $q.reject();
			}
			var deferred = $q.defer();

			$http.post(urls.api('authLogin'), credentials)
				.success(function(user, status, headers, config) {
					if(user != null) {
						localStorageService.set(authStorage, user);
						notify(true);
						deferred.resolve(user);
					}
					else {
						deferred.reject('Login error');
					}
				})
				.error(function(err) {
					deferred.reject('Login error');
				});

			return deferred.promise;
		};

		this.getUser = function() {
			var deferred = $q.defer(),
					user = localStorageService.get(authStorage);

			if(user !== null) {
				deferred.resolve(user);
			}
			else {
				deferred.reject();
			}

			return deferred.promise;
		};

		this.getToken = function() {
			var deferred = $q.defer();
			this.getUser()
				.then(function(user) {
					deferred.resolve(user.token);
				})
				.catch(function(){
					deferred.reject();
				});
			return deferred.promise;
		};

		this.isAnonymous = function() {
			return localStorageService.get(authStorage) === null;
		};

		this.logout = function() {
			var deferred = $q.defer();

			$http.post(urls.api('authLogout'))
				.success(function() {
					localStorageService.remove(authStorage);
					notify(false);
					deferred.resolve();
				})
				.error(function() {
					deferred.reject();
				});

			return deferred.promise;
		};

		function unsubscribe(callback) {
			return function() {
				_.remove(observers, function(x) {
					return x === callback;
				});
			};
		}

		function notify(authorized, fireOnce) {
			if(fireOnce) {
				fireOnce(authorized);
				return;
			}
			_.each(observers, function(callback) {
				callback(authorized);
			});
		}
	}

}());
