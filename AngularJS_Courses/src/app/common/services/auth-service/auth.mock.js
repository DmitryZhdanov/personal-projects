(function(){
	'use strict';

	angular
		.module('app.common')
		.run(run);

	run.$inject = ['$httpBackend', 'urls', '$log'];

	function run($httpBackend, urls, $log) {
		var users = [{
			id: 1,
			login: 'admin',
			password: 'admin'
		}, {
			id: 2,
			login: 'login',
			password: 'password'
		}];

		$httpBackend.whenPOST(urls.mock('authLogin')).respond(function(method, url, data, headers) {
			data = angular.fromJson(data);
			var user = _.find(users, function(x) {
				return x.login === data.login && x.password === data.password;
			});
			if(angular.isUndefined(user)) {
				return [400, {error: {}}, {}];
			}

			return [200, angular.toJson({
				login: user.login,
				token: '48052ac1-daef-f133-d914-d7c4fe25d972'
			}), {}];
		});

		$httpBackend.whenPOST(urls.mock('authLogout')).respond(function(){
			return [200, true, {}];
		});
	}

}());
