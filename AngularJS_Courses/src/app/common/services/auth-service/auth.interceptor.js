(function() {
	'use strict';

	angular
		.module('app.common')
		.factory('authInterceptor', authInterceptor);

	authInterceptor.$inject = ['$q', '$injector'];

	function authInterceptor($q, $injector) {
		var intercep = {};

		// =========================================
		// Add auth token for each request to server
		// =========================================
		intercep.request = function(cfg) {
			var authService = $injector.get('authService'),
				deferred = $q.defer();

			authService.getToken()
				.then(function(token) {
					cfg.headers['session-token'] = token;
				})
				.finally(function() {
					deferred.resolve(cfg);
				});

			return deferred.promise;
		};

		// intercep.responseError = function(response) {
		// 	// Relogin user when token is forgotten
		// 	if(response.status === 419) {	// 419 - session timeout code vrode
		// 		var $http = $injector.get('$http');
		// 		return authService.login().then(function() {
		// 			// После повторной авторизации выполняем первоначальный запрос от пользователя
		// 			return $http(response.config);
		// 		});
		// 	}
		// 	return $q.reject(response);
		// };

		return intercep;
	}

}());
