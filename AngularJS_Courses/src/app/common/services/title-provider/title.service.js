(function(){
	'use strict';

	angular
		.module('app.common')
		.service('titleProvider', titleProvider);

	titleProvider.$inject = ['$rootScope', '$timeout', '$filter', '$state'];

	function titleProvider($rootScope, $timeout, $filter, $state){
		var observers = [],
		title = $state.current.title,
		watcher = $rootScope.$on('$stateChangeSuccess', handleState);

		$rootScope.$on('$destroy', function(){
			watcher();
		});

		return {
			subscribe: subscribe,
			setTitle: setTitle,
			getTitle: getTitle
		};

		function handleState(event, toState){
			if (toState && toState.title) {
				$timeout(function() {
					setTitle(toState.title);
				}, 0, false);
			}
		}

		function getTitle() {
			return title;
		}

		function setTitle(newTitle) {
			title = $filter('normalizeTitle')(newTitle);
			angular.forEach(observers, function(callback){
				callback(title);
			});
		}

		function subscribe(callback) {
			observers.push(callback);
			return {
				unsubscribe: function(){
					_.remove(observers, function(x){
						return x === callback;
					});
				},
				current: title
			};
		}
	}
}());
