(function(){
	'use strict';

	angular
		.module('app.common')
		.service('authorsService', authorsService);

	authorsService.$inject = ['$resource', 'urls'];
	function authorsService($resource, urls) {
		return $resource(urls.api('authors'));
	}

}());
