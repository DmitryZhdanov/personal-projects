(function(){
	'use strict';

	angular
		.module('app.common')
		.run(run);

	run.$inject = ['$httpBackend', 'urls'];

	function run($httpBackend, urls) {
		var auhtors = [{
			id: 1,
			name: 'Eugeniy P.'
		}, {
			id: 2,
			name: 'Vasiliy I.'
		}, {
			id: 3,
			name: 'Fedor Jaa'
		}, {
			id: 4,
			name: 'Ivan O'
		}];

		$httpBackend.whenGET(urls.mock('authors')).respond(function(method, url, data, headers, params) {
			var token = headers['session-token'];
			if(token) {
				// Check token on backend
			}

			return [200, angular.toJson(auhtors), {}];
		});
	}

}());
