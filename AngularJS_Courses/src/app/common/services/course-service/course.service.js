(function () {
	'use strict';

	angular
		.module('app.common')
		.factory('courseService', courseService);

	courseService.$inject = ['urls', '$resource'];
	function courseService(urls, $resource){
		var courseFactory = $resource(urls.api('courses'), {
			course: '@course'
		}, {
			update: {
				method: 'PUT'
			}
		});
		return courseFactory;
	}

}());
