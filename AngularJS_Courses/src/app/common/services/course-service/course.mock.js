(function(){
	'use strict';

	angular
		.module('app.common')
		.run(run);

	run.$inject = ['$httpBackend', 'urls'];

	function run($httpBackend, urls) {
		var courses = [{
			id: 1,
			title: 'Course #1',
			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis fugiat error illum libero labore, nisi sed possimus, aperiam voluptatum et eveniet soluta dicta sint ipsum doloribus dolore aliquam est magni veniam rem, molestias voluptas praesentium. Harum voluptatum quaerat doloribus alias illo, voluptates in officia. Nesciunt.',
			authors: [{
				id: 3,
				name: 'Fedor Jaa'
			}],
			duration: 300,
			date: new Date()
		}, {
			id: 2,
			title: 'Video course 2',
			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus blanditiis atque doloribus nihil quibusdam facere nemo quod, animi quae dolorem recusandae magnam vel rerum iusto, modi nulla. Inventore, repellat, vitae.',
			authors: [
				{
					id: 1,
					name: 'Eugeniy P.'
				}, {
					id: 2,
					name: 'Vasiliy I.'
				}
			],
			duration: 88,
			date: new Date()
		}];


		$httpBackend.whenGET(urls.mock('courses'), undefined, ['course'])
			.respond(function(method, url, data, headers, params) {
				var token = headers['session-token'];
				if(token) {
					// Check token on backend
				}

				var courseId = parseInt(params.course, 10);
				if(isNaN(courseId)) {
					return [200, angular.toJson(courses), {}];
				}
				if(!isNaN(courseId)) {
					var course = _.find(courses, function(x){
						return x.id === courseId;
					});
					if(angular.isDefined(course)) {
						return [200, angular.toJson(course), {}];
					}
					return [400, angular.toJson(course), {}];
				}
			});

		$httpBackend.whenDELETE(urls.mock('courses'), undefined, ['course'])
			.respond(function(method, url, data, headers, params) {
				var token = headers['session-token'];
				if(token) {
					// Check token on backend
				}

				var courseId = parseInt(params.course, 10);
				var removedCourse = _.remove(courses, function(x){
					return x.id === courseId;
				});
				if(angular.isDefined(removedCourse)) {
					return [200, angular.toJson(true), {}];
				}
				return [400, angular.toJson(false), {}];
			});

		$httpBackend.whenPUT(urls.mock('courses'), undefined, undefined, ['course'])
			.respond(function(method, url, updatedCourse, headers, params) {
				var token = headers['session-token'];
				if(token) {
					// Check token on backend
				}

				var courseId = parseInt(params.course, 10);
				if(angular.isUndefined(courseId)) {
					return [400, {}, {}];
				}
				var course = _.find(courses, function(x) {
					return x.id === courseId;
				});

				if(angular.isUndefined(course)) {
					return [400, {}, {}];
				}
				angular.extend(course, angular.fromJson(updatedCourse));
				return [200, angular.toJson(course), {}];
			});

		$httpBackend.whenPOST(urls.mock('courses'))
			.respond(function(method, url, data, headers, params) {
				var course = angular.fromJson(data);
				course.id = courses[courses.length - 1].id + 1;
				courses.unshift(course);
				return [202, course, {}];
			});
	}

}());
