(function(){
	'use strict';

	angular
		.module('app.common')
		.service('spinnerService', spinnerService);

	function spinnerService() {
		var count = 0;

		return {
			transitionStart: function() {
				if (++count < 0) {
					showSpinner();
				}
			},
			transitionEnd: function() {
				if(--count <= 0) {
					hideSpinner();
				}
			}
		};

		function showSpinner() {

		}

		function hideSpinner() {

		}
	}
}());
