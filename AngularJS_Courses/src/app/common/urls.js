(function () {
	'use strict';

	angular
		.module('app.common')
		.factory('urls', function ($window, settings) {
			var urls;

			urls = {
				courses: {
					api: function(term) {
						return 'api/courses/:course';
					},
					mock: /api\/courses(?:\/)?(.+)?/,
					isMocked: true //API implemented
				},
				authLogin: {
					api: '/auth/login',
					mock: /auth\/login/,
					isMocked: true
				},
				authLogout: {
					api: '/auth/logout',
					mock: /auth\/logout/,
					isMocked: true
				},
				authors: {
					api: '/api/authors',
					mock: /api\/authors/,
					isMocked: true
				}
			};

			function api(name) {
				var args = _.slice(arguments, 1, arguments.length),
					apiUrl = urls[name].api,
					rawUrl = angular.isString(apiUrl) ? apiUrl : apiUrl.apply({}, args);

				if (settings.testMode || urls[name].isMocked) {
					return rawUrl;
				}
				return $window.location.protocol + settings.webApiUrl + rawUrl;
			}

			function mock(name) {
				return urls[name].mock;
			}

			return {
				api: api,
				mock: mock
			};
		});
}());
