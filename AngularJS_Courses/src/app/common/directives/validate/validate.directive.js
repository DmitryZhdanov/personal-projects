(function(){
	'use strict';

	angular
		.module('app.common')
		.directive('validate', validate);

	validate.$inject = ['$window', '$document'];
	function validate($window, $document) {
		var validatorTypes = {
			'time': {
				validateChar: function(data) {
					return /(\d+)/g.test(data);
				},
				maxlength: 5
			}
		};

		function getSelectionText() {
			var text = '';
			if ($window.getSelection) {
				text = $window.getSelection().toString();
			}
			else if ($document.selection && $document.selection.type !== 'Control') {
				text = $document.selection.createRange().text;
			}
			return text;
		}
		var ignoreKeys = ['Enter', 'Escape', 'Control', 'Backspace', 'Shift', 'Alt', 'F5'];
		return {
			restrict: 'A',
			scope: {},
			link: function(scope, elem, attr) {
				var type = attr.validate;
				elem.on('keypress paste', function(e){
					var currentLength = this.value.length;

					if(angular.isDefined(validatorTypes[type].maxlength) && currentLength > validatorTypes[type].maxlength - 1 && getSelectionText().length === 0) {
						return false;
					}

					switch(e.type) {
						case 'paste': {
							var clipboardData = (e.originalEvent.clipboardData || $window.clipboardData).getData('Text');
							if(angular.isDefined(validatorTypes[type].maxlength) && clipboardData.length > validatorTypes[type].maxlength - 1) {
								this.value = clipboardData.substr(0, validatorTypes[type].maxlength - 1);
								return false;
							}
							return validatorTypes[type].validateChar(clipboardData);
						} break;
						case 'keypress': {
							var key = String.fromCharCode(e.keyCode);
							return ignoreKeys.indexOf(key) > 0 ? true : validatorTypes[type].validateChar(key);
						} break;
						default: {
							return false;
						}
					}
				});
			}
		};
	}
}());
