(function() {
	'use strict';

	angular
		.module('app.common')
		.directive('connectedList', connectedList);

	function connectedList() {

		function directiveController(){
			var vm = this;
			vm.moveItem = moveItem;

			vm.lists = {
				filter: {
					current: '',
					destination: ''
				},
				selected: {
					current: [],
					destination: []
				}
			};

			function moveItem(from, to, clear) {
				angular.forEach(from, function(item){
					to.push(item);
				});
				_.pullAll(clear, from);
				from.length = 0;
			}
		}

		function link() {

		}

		return {
			bindToController: true,
			controller: directiveController,
			controllerAs: 'vm',
			link: link,
			restrict: 'E',
			replace: true,
			templateUrl: 'app/common/directives/connected-list/connected-list.template.html',
			scope: {
				id: '@',
				name: '@',
				sourceList: '=source',
				destinationList: '=destination'
			}
		};
	}

}());
