(function(){
	'use strict';

	angular
		.module('app.common')
		.directive('inputDuration', inputDuration);

		inputDuration.$inject = ['$filter'];

		function inputDuration($filter) {
			return {
				restrict: 'A',
				link: link,
				require: 'ngModel'
			};

			// ========================================================
			// $modelValue is needed at initialization
			// so we should init directive after ngModel initialization
			// ========================================================
			function link(scope, elem, attr, ngModel) {
				var unregister = scope.$watch(function(){
					return ngModel.$modelValue;
				}, initialize),
					timeFilter = $filter('time'),
					helper = angular.element('<span class="inline-helper"></span>');

				function initialize(value) {
					ngModel.$setViewValue(value);
					unregister();
					updateFilter();
					elem.after(helper);
					elem.on('keyup', updateFilter);
				}
				function updateFilter() {
					helper.text(timeFilter(ngModel.$modelValue));
				}
			}

		}
}());
