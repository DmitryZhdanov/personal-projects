(function(){
	'use strict';

	angular
		.module('app.common')
		.directive('concatList', concatList);

	function concatList() {
		return {
			restrict: 'A',
			scope: {},
			link: function(scope, elem, attrs, ctrl) {
				var separator = attrs.separator || ', ',
					byProp = attrs.by,
					list = scope.$eval(attrs.concatList);

				elem.html(_.map(list, function(x) {
					return byProp ? x[byProp] : x;
				}).join(separator));
			}
		};
	}
}());
