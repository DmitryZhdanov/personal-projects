(function () {
	'use strict';

	angular
		.module('app.common')
		.directive('breadcrumbs', breadcrumbs);

	breadcrumbs.$inject = ['$state', '$rootScope', '$stateParams'];

	function breadcrumbs($state, $rootScope, $stateParams) {

		function directiveController(){
			var vm = this;

			vm.breadcrumbs = [
				{
					title: 'Courses',
					state: 'shell.courses.list'
				},
				{
					title: 'Item'
				}
			];
		}

		function link(scope, elem, attrs, ctrl) {
			var unregister = $rootScope.$on('$stateChangeSuccess', function () {
				var current = $state.current,
					currentTitle = $stateParams.title,
					parent = $state.get('^');

				ctrl.breadcrumbs = [];
				ctrl.breadcrumbs.unshift({
					title: currentTitle
				});
				ctrl.breadcrumbs.unshift({
					title: parent.breadcrumbs,
					state: current.siteMapParent
				});

			});
			scope.$on('$destroy', function() {
				unregister();
			});
		}

		return {
			bindToController: true,
			templateUrl: 'app/common/directives/breadcrumbs/breadcrumbs.template.html',
			controller: directiveController,
			controllerAs: 'vm',
			link: link,
			restrict: 'E',
			scope: {}
		};
	}

}());
