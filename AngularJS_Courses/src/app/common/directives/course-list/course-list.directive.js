(function () {
	'use strict';

	angular
		.module('app.common')
		.directive('courseList', courseList);

	courseList.$inject = ['$modal'];

	function courseList($modal) {

		function directiveController() {
			var vm = this;
			vm.deleteCourse = deleteCourse;

			init();

			function init() {

			}

			function deleteCourse(course) {
				var options = {
					controller: 'modalCtrl',
					controllerAs: 'vm',
					size: 'md',
					templateUrl: 'app/modals/confirm-modal.template.html',
					resolve: {
						data: function() {
							return {
								header: 'Confirm action',
								message: 'Are you sure want to delete this course?',
								buttons: {
									save: 'Delete'
								}
							};
						}
					}
				};
				$modal.open(options).result.then(function() {
					vm.onDelete({id: course.id});
				});
			}
		}

		function link(scope, elem, attrs, ctrl) {

		}

		return {
			bindToController: true,
			templateUrl: 'app/common/directives/course-list/course-list.template.html',
			controller: directiveController,
			controllerAs: 'vm',
			link: link,
			restrict: 'E',
			scope: {
				courses: '=',
				onDelete: '&'
			}
		};
	}

}());
