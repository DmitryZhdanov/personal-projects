(function(){
	'use strict';

	angular
		.module('app.common')
		.directive('inputDate', inputDate);

		inputDate.$inject = ['$filter'];

		function inputDate($filter) {
			return {
				restrict: 'A',
				link: link,
				require: 'ngModel'
			};

			function link(scope, elem, attr, ngModel) {
				var format = attr.inputDate;
				ngModel.$formatters.push(function(value) {
					if(angular.isDefined(value)) {
						return $filter('date')(new Date(value), format);
					}
				});
				ngModel.$parsers.push(function(value) {
					var d = value.substr(0, 2),
						m = value.substr(2, 2),
						y = value.substr(4);

					return new Date(y, m, d);
				});
			}

		}
}());
