(function() {
	'use strict';

	angular
		.module('app')
		.config(config);

	config.$inject = ['settings', 'localStorageServiceProvider', '$provide', '$httpProvider', 'toastrConfig', 'uiMask.ConfigProvider'];

	function config(settings, localStorageServiceProvider, $provide, $httpProvider, toastrConfig, uiMaskConfigProvider) {
		// ====================
		// Local storage config
		// ====================
		localStorageServiceProvider
			.setPrefix('courseApp')
			// 1. LocalStorageModule.notification.setitem / def true
			// 2. LocalStorageModule.notification.removeitem / def false
			.setNotify(false, false);

		// ===============================================
		// Auth interceptor
		// Check token's expiration for non-route requests
		// ===============================================
		$httpProvider.interceptors.push('authInterceptor');

		// ======================================
		// Delay mock responses from $httpBackend
		// ======================================
		$provide.decorator('$httpBackend', ['$delegate', '$timeout', function($delegate, $timeout) {
			var $httpBackend = function() {
				var args = arguments;
				$timeout(function() {
					return $delegate.apply(null, args);
				}, 0, false);
			};
			angular.extend($httpBackend, $delegate);
			return $httpBackend;
		}]);

		angular.extend(toastrConfig, {
			autoDismiss: true,
			newestOnTop: true,
			positionClass: 'toast-top-right',
			progressBar: true
		});

		uiMaskConfigProvider.clearOnBlur(false);

	}

}());
