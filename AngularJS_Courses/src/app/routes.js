(function () {
	'use strict';

	angular
		.module('app')
		.config(routes);

	routes.$inject = ['$stateProvider', '$urlRouterProvider'];
	function routes($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('shell', {
				url: '',
				abstract: true,
				controller: 'shellCtrl',
				controllerAs: 'vm',
				templateUrl: 'app/pages/shell.template.html'
			})
			.state('shell.login', {
				url: '/login',
				templateUrl: 'app/pages/login/login.template.html',
				controller: 'loginCtrl',
				controllerAs: 'vm',
				allowAnonymous: true,
				title: 'Signing in'
			})
			// ======================
			// Course routes
			// ======================
			.state('shell.courses', {
				abstract: true,
				url: '/courses',
				controller: 'coursesCtrl',
				controllerAs: 'vm',
				breadcrumbs: 'Courses',
				templateUrl: 'app/pages/courses/courses.template.html'
			})
			.state('shell.courses.list', {
				url: '',
				templateUrl: 'app/pages/courses/course-list/course-list.template.html',
				controller: 'courseListCtrl',
				controllerAs: 'vm',
				title: 'Courses',
				breadcrumbs: '',
				resolve: {
					courseListQ: ['courseService', function(courseService) {
						return courseService.query();
					}]
				}
			})
			.state('shell.courses.edit', {
				url: '/{id:[0-9]+|new}',
				templateUrl: 'app/pages/courses/edit-course/edit-course.template.html',
				controller: 'editCourseCtrl',
				controllerAs: 'vm',
				title: '',
				breadcrumbs: '',
				siteMapParent: 'shell.courses.list',
				params: {
					id: 'new'
				},
				resolve: {
					courseQ: ['courseService', 'authorsService', '$stateParams', '$state', '$q', function(courseService, authorsService, $stateParams, $state, $q) {
						var courseId = parseInt($stateParams.id, 10);

						if(!isNaN(courseId)) {
							var defer = $q.defer();
							courseService.get({course: courseId}, function(course) {
								defer.resolve(course);
							}, function() {
								$state.go('shell.courses.list');
								defer.reject();
							});

							return defer.promise;
						}
						// new course
						return null;
					}],
					authorsQ: ['$q', 'authorsService', function($q, authorsService) {
						var defer = $q.defer();

						authorsService.query(function(data) {
							defer.resolve(data);
						}, function() {
							defer.reject();
						});

						return defer.promise;
					}]
				},
				onEnter: ['courseQ', '$stateParams', 'titleProvider', '$state', function(courseQ, $stateParams, titleProvider, $state) {
					var title;

					if(courseQ !== null) {
						title = courseQ.title;
						$stateParams.title = courseQ.title;
					}

					else {
						title = 'New course';
						$stateParams.title = 'New course';
					}

					titleProvider.setTitle(title);
				}]
			});

		$urlRouterProvider.otherwise(function($injector) {
			$injector.get('$state').go('shell.courses.list');
		});

	}
}());
