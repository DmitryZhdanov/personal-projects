(function(){
	'use strict';

	angular
		.module('app')
		.controller('loginCtrl', loginCtrl);

	loginCtrl.$inject = ['authService', '$log', '$state'];
	function loginCtrl(authService, $log, $state){
		var vm = this;
		vm.regex = {
			login: '\[a-zA-Z]+',
			password: '\[a-zA-Z0-9]+'
		};
		vm.credentials = {
			login: '',
			password: ''
		};
		vm.loginError = false;

		vm.login = function(valid) {
			if(valid) {
				authService.login(vm.credentials).then(function(response) {
					vm.loginError = false;
					$state.go('shell.courses.list');
				}, function(response) {
					vm.loginError = true;
					vm.credentials.password = '';
				});
			}
		};
	}

}());
