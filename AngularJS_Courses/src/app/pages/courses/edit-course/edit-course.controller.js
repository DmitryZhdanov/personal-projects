(function(){
	'use strict';

	angular
	.module('app')
	.controller('editCourseCtrl', editCourseCtrl);

	editCourseCtrl.$inject = ['courseService', '$rootScope', '$scope', '$modal', 'courseQ', 'authorsQ', '$state', 'toastr'];

	function editCourseCtrl(courseService, $rootScope, $scope, $modal, courseQ, authorsQ, $state, toastr) {
		var vm = this;
		vm.course = courseQ || {
			title: '',
			description: '',
			date: '',
			duration: '',
			authors: []
		};
		vm.authorsSource = _.differenceBy(authorsQ, vm.course.authors, 'id');
		vm.submit = submit;

		function submit(course, form) {
			if(form.$valid) {
				var action = courseQ ? 'update' : 'save';
				courseService[action]({course: course.id}, course, function() {
					$state.go('shell.courses.list', {reload: true});
					if(action === 'save') {
						toastr.success('New course has been added.');
						return;
					}
					toastr.success('Course has been updated.');
				}, function() {
					toastr.error('Unable to save course.');
				});
			}
			else {
				var options = {
					controller: 'modalCtrl',
					controllerAs: 'vm',
					size: 'sm',
					templateUrl: 'app/modals/info-modal.template.html',
					resolve: {
						data: function() {
							return {
								header: 'Error',
								message: 'Not all fields are filled'
							};
						}
					}
				};
				$modal.open(options);
			}
		}
	}
}());
