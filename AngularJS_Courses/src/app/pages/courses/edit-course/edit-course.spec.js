/// <reference path="../../testHelpers/_reference.js" />
/// <reference path="edit-course.controller.js" />
(function () {
	'use strict';

	describe('editCourseCtrl', function () {
		beforeEach(module('app'));
		beforeEach(module('app.common'));

		var vm,
			courseService,
			$state,
			$rootScope,
			$scope,
			authorsQ,
			courseQ,
			toastr,
			$q,
			$modal;

		beforeEach(inject(function ($controller, _$modal_, _$q_, _$rootScope_, _toastr_) {
			$state = {
				go: function () {
					return true;
				}
			};
			$rootScope = _$rootScope_;
			$scope = $rootScope.$new();
			$modal = _$modal_;
			courseQ = [
				{
					title: 'Course #1',
					description: 'Desc',
					authors: [{
						id: 3,
						name: 'Fedor Jaa'
					}],
					duration: 300,
					date: new Date()
				}
			];
			authorsQ = {};
			toastr = _toastr_;
			$q = _$q_;
			courseService = {
				update: function(course) {
					course.id = 1;
					return $q.when([course]);
				}
			};
			vm = $controller('editCourseCtrl', {
				courseService: courseService,
				$rootScope: $rootScope,
				$scope: $scope,
				$modal: $modal,
				courseQ: courseQ,
				authorsQ: authorsQ,
				$state: $state,
				toastr: toastr
			});

			spyOn(courseService, 'update').and.callThrough();
			spyOn($state, 'go');
			spyOn(toastr, 'error');
			spyOn(toastr, 'success');
			spyOn($modal, 'open').and.callThrough();
			$scope.$digest();
		}));

		it('should properly initialize', function () {
			expect(vm.course).toBeDefined();
			expect(vm.authorsSource).toBeDefined();
			expect(vm.submit).toBeDefined();
		});

		it('should call service\'s  update course method', function () {
			vm.course = courseQ;
			vm.submit(vm.course, {
				$valid: true
			});
			expect(courseService.update).toHaveBeenCalled();
		});

		it('should open error modal dialog for not valid form', function () {
			vm.course = courseQ;
			vm.submit(vm.course, {
				$valid: false
			});
			expect($modal.open).toHaveBeenCalled();
		});
	});

}());
