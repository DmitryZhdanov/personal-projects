(function(){
	'use strict';

	angular
		.module('app')
		.controller('courseListCtrl', courseListCtrl);

	courseListCtrl.$inject = ['courseListQ', 'courseService', '$filter', '$log'];

	function courseListCtrl(courseListQ, courseService, $filter, $log){
		var vm = this,
			searchFilter = $filter('search');
		vm.courses = courseListQ;
		vm.onDelete = onDelete;
		vm.searchTerm = '';
		vm.search = search;

		function onDelete(id) {
			courseService.remove({course: id}).$promise
				.then(function(res) {
					_.remove(vm.courses, function(x) {
						return x.id === id;
					});
				})
				.catch(function() {
					$log.log('Unable to remove');
				});
		}

		function search(term) {
			var filtered = searchFilter(courseListQ, {title: term}, '$index');
			vm.courses = filtered;
		}
	}

}());
