/// <reference path="../../testHelpers/_reference.js" />
/// <reference path="course-list.controller.js" />
(function () {
	'use strict';

	describe('courseListCtrl', function () {
		beforeEach(module('app'));
		beforeEach(module('app.common'));

		var vm,
			courseListQ,
			courseService,
			$filter,
			$log,
			$q;

		beforeEach(inject(function ($controller, _$log_, _$q_, _$filter_) {
			courseListQ = [
				{
					id: 1,
					title: 'Course #1',
					description: 'Desc',
					authors: [{
						id: 3,
						name: 'Fedor Jaa'
					}],
					duration: 300,
					date: new Date()
				}, {
					id: 2,
					title: 'Course #2',
					description: 'Desc',
					authors: [{
						id: 3,
						name: 'Fedor Jaa'
					}],
					duration: 300,
					date: new Date()
				}
			];

			$q = _$q_;
			courseService = {
				remove: function(course) {
					_.remove(vm.courses, function(x) {
						return x.id === course.course;
					});
					return {
						$promise: $q.when(true)
					};
				}
			};

			$log = _$log_;

			$filter = _$filter_;

			vm = $controller('courseListCtrl', {
				courseListQ: courseListQ,
				courseService: courseService,
				$filter: $filter,
				$log: $log
			});

			spyOn(courseService, 'remove').and.callThrough();

		}));

		it('should properly initialize', function () {
			expect(vm.courses.length).toBe(2);
		});

		it('should properly delete course item', function () {
			var oldLength = vm.courses.length;
			vm.onDelete(1);
			expect(vm.courses.length).toBe(oldLength - 1);
		});

		it('should call service on delete', function () {
			vm.onDelete(1);
			expect(courseService.remove).toHaveBeenCalled();
		});

	});

}());
