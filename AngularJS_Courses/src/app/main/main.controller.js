(function() {
	'use strict';

	angular
		.module('app')
		.controller('mainCtrl', mainCtrl);

	mainCtrl.$inject = ['authService', '$state'];

	function mainCtrl(authService, $state){
		var vm = this;
		vm.logout = logout;
		vm.authorizedUser = null;

		authService.subscribe(authHandler);

		function authHandler(auth) {
			if(auth) {
				authService.getUser().then(function(user) {
					vm.authorizedUser = user;
				});
			}
		}

		function logout() {
			authService.logout().then(function() {
				vm.authorizedUser = null;
				$state.reload();
			});
		}

	}
}());
